\section{Design of \uichex}
\label{SUPOR:sec:design}

In this section, we first present our threat model,
followed by an overview of \uichex.
Then, we describe each component of \uichex in details.

\subsection{Threat Model}

We position \uichex as a static UI analysis tool for detecting
sensitive user inputs.
Instead of focusing on malicious apps that deliberately evade detection,
\uichex is designed for efficient and scalable screening of a large number of apps.
Most of the apps in the app markets are legitimate, 
whose developers try to monetize by gaining user popularity,
even though some of them might be a little bit aggressive on
exploiting user privacy for revenue. 
Malware can be detected by existing works~\cite{Grace:2012:RSA:2307636.2307663, Yajin:12, Arp:14}, which is out
of scope of this paper.

Though the developers sometimes dynamically generate UI elements in the code
other than defining the UI elements via layout files, we focus on identifying
sensitive user inputs statically defined in layout files in this work.

\subsection{Overview}
Figure~\ref{SUPOR:fig:workflow} shows the workflow of \uichex.
\uichex consists of three major components:
\emph{Layout Analysis}, \emph{UI Sensitiveness Analysis}, and \emph{Variable Binding}.
The layout analysis component accepts an APK file of an app,
parses the layout files inside the APK file,
and renders the layout files containing input fields.
Based on the outcome of UI rendering,
the UI sensitiveness analysis component associates text labels to the input fields,
and determines the sensitiveness of the input fields
by checking the texts in the text labels against a predefined sensitive keyword dataset (Section~\ref{SUPOR:subsec:design:keyword}).
The variable binding component then searches the code
to identify the variables that store the values of the sensitive input fields.
With variable binding, existing research efforts in studying
the privacy related topics on predefined well-known sensitive data sources
can be applied to sensitive user inputs.
For example, one can use taint analysis to detect disclosures of sensitive user inputs
or other privacy analysis to analyze vulnerabilities of sensitive user inputs in the apps.
Next we describe each component in detail.

\begin{figure}[t]
	\centering
	\includegraphics[angle=-90,width=.98\textwidth,clip]{figs/workflow.eps}
	\caption{Overview of \uichex.} \label{SUPOR:fig:workflow}
\end{figure}

\subsection{Layout Analysis}
The goal of the layout analysis component is to render the UIs of an Android app,
and extract the information of input fields: types, hints, and absolute coordinates,
which are later used for the UI sensitiveness analysis.

As we discussed in Section~\ref{SUPOR:subsec:uisensitiveness},
if we cannot determine the sensitiveness of an input field based on its type and hint,
we need to find a text label that describes the purpose of the input field.
%However, it is a challenging task to associate a text label with an input field.
%When an input field is shown in the screen,
%usually there are also multiple text labels shown in the same screen.
%As there are no direct indicators in the layout file that specify which text labels describing which input fields,
%each of these text labels is possible to describe the purpose of the input field.
%Thus, we need to figure out which text labels are most likely to describe which input fields.
From the \emph{user's perspective},
the text label that describes the purpose of an input field
must be \emph{physically close to the input field} in the screen;
otherwise the user may correlate the text label with other input fields and provide inappropriate inputs.
Based on this insight,
the layout analysis component renders the UIs as if the UIs are rendered in production runs,
mimicking how users look at the UIs.
Based on the rendered UIs, the distances between text labels and input fields are computed,
and these distances are used later to find the best descriptive text labels for each input field.
We next describe the two major steps of the layout analysis component.

The first step is to identify which layout files contain input fields
by parsing the layout files in the APK of an Android app.
In this work, we focus on input fields of the type \texttt{EditText} and
all possible sub-types, including custom widgets in the apps.
Each input field represents a potential sensitive source.
However, according to our previous discussion,
the sensitiveness cannot be easily determined by analyzing only the layout files.
Thus, all the files containing input fields are used in the second step for UI rendering.

The second step is to obtain the coordinate information of the input fields
by rendering the layout files.
Using the rapid UI development kit provided by Android,
the layout analysis component can effectively render standard UI widgets.
For custom widgets that require more complex rendering,
the layout analysis component renders them by providing the closest library superclass to obtain the best result. 	
After rendering a layout file, the layout analysis component obtains a UI model,
which is a tree-structure model
where the nodes are UI widgets and
the edges describe the parent-child relationship between UI widgets.
Figure~\ref{SUPOR:fig:uitree} shows the UI model obtained
by rendering the layout file in Figure~\ref{SUPOR:fig:layoutxml}.
For each rendered UI widget,
the coordinates are relative to its parent container widget.
Such relative coordinates cannot be directly used for measuring the distances
between two UI widgets,
and thus \uichex converts the relative coordinates to absolute coordinates with regards to the screen size.

\begin{figure}[ht]
	\centering
	\includegraphics[angle=-90,scale=0.9,clip]{figs/uitree.eps}
	\caption{\linespread{1}\selectfont UI model for Figure~\ref{SUPOR:fig:moti_example} on 480x800 screen.
	Only the ID, relative coordinates and text of the widgets are presented here.}
	\label{SUPOR:fig:uitree}
\end{figure}

\textbf{Coordinate Conversion.}
\uichex computes the absolute coordinates of each UI widget level by level,
starting with the root container widget.
For example, in Figure~\ref{SUPOR:fig:uitree}, the root container widget is a {\tt LinearLayout},
and its coordinates are (0, 50, 480, 752),
representing the left, top, right, and bottom corners.
There is no need to convert the coordinates of the root UI widget,
since its coordinates are relative to the top left corner of the screen,
and thus are already absolute coordinates.
For other UI widgets, \uichex computes their absolute coordinates
based on their relative coordinates and their parent container's absolute coordinates.
For example, the relative coordinates of the second UI widget, {\tt TextView}, are (16, 16, 60, 33).
Since it is a child widget of the root UI widget,
its absolute coordinates is computed as (16, 66, 60, 83).
This process is repeated until the coordinates of every UI widget are converted.
\eat{
Since it is a child widget of the root LinearLayout,
to compute the absolute coordinate,
the left and right coordinates should be horizontally move to the right (+0)
based on the left coordinate of the root LinearLayout,
and the top and bottom coordinates should be moved down (+50) based on the top coordinate
of the root LinearLayout.}

In addition to coordinate conversion, \uichex collects other information of the UI widgets,
such as the texts in the text labels and the attributes for input fields
(e.g., {\tt ID} and {\tt inputType}).
\eat{
For example, in Figure~\ref{SUPOR:fig:uitree},
\uichex identifies the text label with the ID \texttt{lbl\_password} (0x7f080000 in binary code and at run time),
containing text ``Please enter your password''.
\uichex also identifies four input fields and their their {\tt inputType} attributes.}


\subsection{UI Sensitiveness Analysis}
Based on the information collected from the layout analysis, 
the UI sensitiveness analysis component determines whether a given input field
contains sensitive information.
This component consists of three major steps.

First, if the input field has been assigned with certain attributes like
{\tt android:input}-{\tt Type="textPassword"}, it is directly considered as sensitive.
With such attribute, the original inputs on the UI are concealed after users type them.
In most cases these inputs are passwords.
%We consider such information is sensitive to the user.

Second, if the input field contains any hint (\ie tooltip), \eg ``Enter Password Here'',
the words in the hint are checked:
if it contains any keyword in our sensitive keyword dataset, the input field is considered sensitive;
otherwise, the third step is required to determine its sensitiveness.


\begin{algorithm}[t]
\begin{algorithmic}[1]
\caption{UI Widget Sensitiveness Analysis}
\label{SUPOR:alg:uisensitiveness}
\REQUIRE $I$ as an input field, $S$ as a set of text labels, $KW$ as a pre-defined sensitive keyword dataset
\ENSURE $R$ as whether $I$ is sensitive
	
			\STATE Divide the UI plane into \emph{nine} partitions based on $I$'s boundary
			\FORALL{$L \in S$}
				\STATE $score$ = $0$
				\FORALL{$(x,y) \in L$}
					\STATE $score$ += $distance(I, x, y) * posWeight(I, x, y)$
				\ENDFOR
				\STATE $L.score$ = $score$ $/$ $L.numOfPixels$
			\ENDFOR
			%\STATE sort $S$ by scores
			%\STATE $R$ = $False$
			%\WHILE{$L \in S$ and $L.score$ $\le$ $threshold$}
				%\IF{$L.text$ matches $KW$}
					%\STATE $R$ = $True$ $|$ $R$
				%\ENDIF
			%\ENDWHILE
			\STATE $T$ = $min(S)$
			%\IF{$T.text$ matches $KW$}
				%\STATE $R$ = $True$
			%\ELSE
				%\STATE $R$ = $False$
			%\ENDIF
			\STATE $R$ = $T.text$ matches $KW$
			
\end{algorithmic}
\end{algorithm}

Third, \uichex identifies the text label that describes the purpose of the input field,
and analyzes the text in the label to determine the sensitiveness.
In order to identify text labels that are close to a given input field,
we provide an algorithm to compute correlation scores for each pair of a text label
and an input field based on their distances and relative positions.

The details of our algorithm is shown in Algorithm~\ref{SUPOR:alg:uisensitiveness}.
At first, \uichex divides the UI plane into nine partitions based
on the boundaries of the input field.
Figure~\ref{SUPOR:fig:nine_parts} shows the nine partitions divided by an input field.
Each text label can be placed in one or more partitions,
and the input field itself is placed in the central partition.
For a text label, we determine how it is correlated to an input field
by computing how each pixel in a text label is correlated to the input field (Line 4).
The correlation score for a pixel consists of two parts (Line 5).
The first part is the Euclidean distance from the pixel to the input field,
computed using the absolute coordinates.
The second part is a weight based on their relative positions, 
\ie which of the nine partitions the widget is in.
We build the position-based weight function based on our empirical observations:
if the layout of the apps is top-down and left-right arranged,
the text label that describes the input field is usually placed at left or on top of the input field
while the left one is more likely to be the one if it exists.
We assign smallest weight to the pixels in the left partition and second smallest for the top partition.
The right-bottom partition is least possible so we give the largest weight to it.
The detailed weights for each partition is shown in Figure~\ref{SUPOR:fig:nine_parts}.
Based on the correlation scores of all the pixels,
our algorithm uses the average of the correlation scores 
as the correlation score for the pair of the text label and the input field (Line 7).
The label with smaller correlation score is considered more correlated to the input field.

After the correlation scores for all text labels are computed,
\uichex selects the text label that has the smallest score as the descriptive text label for the input field,
and uses the pre-defined sensitive keyword dataset to determine if the label contains any sensitive keyword.
If yes, the input field is considered as sensitive.
%and associate the text labels whose scores are not bigger than
%the pre-defined \emph{threshold} to the input field.
%If any of these labels contain sensitive keywords from our pre-defined sensitive keyword dataset,
%the input field is considered as sensitive.
%The one with the smallest score is then selected to be the text label that describes the input field.


\begin{figure}[t]
	\centering
	\includegraphics[angle=-90,width=0.9\textwidth,clip]{figs/nine_parts.eps}
	\caption{The partition of the UI is based on the boundary of the input field.}
	\label{SUPOR:fig:nine_parts}
\end{figure}

%The algorithm is presented in Algorithm~\ref{SUPOR:alg:uisensitiveness}.



\begin{figure}[t]
	\centering
\begin{tikzpicture}[auto]
	%\draw [draw, thin] (-6,-3) grid (6,3);
	\node [rectangle, draw, thin] {\includegraphics[width=0.75\textwidth,clip]{figs/uisensitiveness_example.eps}};
	\draw [draw,thick] (-5.68,1.65) rectangle (5.69,0.08);
	\draw [draw,thick] (-5.68,-1.08) rectangle (5.69,-2.6);
\end{tikzpicture}
	\caption{Example for UI widget sensitiveness analysis.} \label{SUPOR:fig:uisensitiveness_example}
\end{figure}

%\begin{figure}[t]
%	\centering
%	\includegraphics[width=0.75\textwidth,clip]{figs/uisensitiveness_example.eps}
%	\caption{Example for UI widget sensitiveness analysis.} \label{SUPOR:fig:uisensitiveness_example}
%\end{figure}

\begin{table}[t]
	\centering
	\caption{Scores of the text labels in Figure~\ref{SUPOR:fig:uisensitiveness_example}.}
		\label{SUPOR:table:uisens_scores}%\small
	\begin{tabular}{| r || r | r |}
		\hline
		     & First Name & Last Name \\ \hline \hline
		1\textsuperscript{st} input field & 46.80 & 218.81  \\ \hline
		2\nd input field & 211.29& 46.84  \\ \hline
	\end{tabular}
	\vspace{0.2in}
\end{table}


\textbf{Example.}
%The sensitiveness of the input fields in Figure~\ref{SUPOR:fig:moti_example} can be
%directly determined by the first step because their attributes indicate that they accept passwords.
%Thus there is no need to apply Algorithm~\ref{SUPOR:alg:uisensitiveness} on the example.
Figure~\ref{SUPOR:fig:uisensitiveness_example} shows an example UI that
requires Algorithm~\ref{SUPOR:alg:uisensitiveness} for sensitiveness analysis.
This example shows a UI that requests a user to enter personal information.
This UI contains two input fields and two text labels.
Neither can \uichex determine the sensitiveness through their attributes,
nor can \uichex use any hint to determine the sensitiveness.
\uichex then applies Algorithm~\ref{SUPOR:alg:uisensitiveness} on these two input fields
to compute the correlation scores for each pair of text labels and input fields. 
The correlation scores are shown in Table~\ref{SUPOR:table:uisens_scores}.
According to the correlation scores,
\uichex associates ``First Name'' to the first input field and ``Last Name'' to
the second input field.
Since our keyword dataset contains keywords ``\emph{first name}''
and ``\emph{last name}'' for personal information,
\uichex can declare the two input fields are sensitive.

Repeating the above steps for every input field in the app,
\uichex obtains a list of sensitive input fields.
It assigns an contextual ID to each sensitive input field in the form of \texttt{<Layout\_ID, Widget\_ID>},
where \texttt{Layout\_ID} is the ID of the layout that contains the input field
and \texttt{Widget\_ID} is the ID of the input field (\ie the value of the attribute ``\texttt{android:id}'').
%These contextual IDs of sensitive input fields are later used by the variable binding component to find the source locations.



\subsection{Variable Binding} \label{SUPOR:subsec:design:taintanalysis}
With the sensitive input fields identified in the previous step,
the variable binding component performs context-sensitive analysis to bind the input fields to the variables in the code.
The sensitive input fields are identified using contextual IDs, 
which include layout IDs and widget IDs.
These contextual IDs can be used to directly locate input fields from the XML layout files.
To find out the variables that store the values of the input fields,
\uichex leverages the binding mechanism provided by Android to load the UI layout 
and bind the UI widgets with the code.
Such a binding mechanism enables \uichex to associate input fields with the proper variables.
We refer to these variables the \emph{widget variables} that are bound to the input fields.

The variable binding component identifies the instances of the input fields 
in a context-insensitive fashion
via searching the code using the APIs provided by the rapid UI development kit of Android.
As shown in Section~\ref{SUPOR:subsec:ui},
\texttt{findViewById(ID)} is an API that loads a UI widget to the code.
Its argument \texttt{ID} is the numeric ID that specifies which widget defined in the XML to load.
Thus, to identify the instances of the input fields,
\uichex searches the code for such method calls,
and compare their arguments to the widget IDs of the sensitive input fields.
If the arguments match any widget ID of the sensitive input fields,
the return values of the corresponding \texttt{findViewById(ID)} are considered as the widget variables for the sensitive input fields.
%Any subsequent method calls that retrieve the values from the input fields
%through the widget variables are considered as taint sources.

One problem here is that developers may assign the same widget ID to UI widgets in different layout files,
and thus different UI widgets are associated with the same numeric ID in the code.
Our preliminary analysis on 5000 apps discovers that
about 22\% of the identified sensitive input fields
have duplicate IDs within the corresponding apps.
Since the context-insensitive analysis cannot distinguish the duplicate widget IDs between layout files inside an app,
a lot of false positives will be presented.

To reduce false positives,
\uichex adds context-sensitivity into the analysis,
associating widget variables with their corresponding layouts.
Similar to loading a widget, the rapid UI development kit provides APIs
to load a UI layout into the code.
For example, \texttt{setContentView(ID)} with a numeric ID as the argument
is used to load a UI layout to the code, as shown at Line 6 in Figure~\ref{SUPOR:fig:code_motivating}.
%This API is a member method call of an activity object.
%Each time it is invoked, the UI layout of the screen is changed for the activity.
Any subsequent \texttt{findViewById} with the ID \texttt{WID} as the argument
returns the UI widget identified by \texttt{WID} in the newly loaded UI layout,
not the UI widget identified by \texttt{WID} in the previous UI layout.
Thus, to find out which layout is associated with a given widget variable,
\uichex traces back to identify the closest method call that loads a UI layout\footnote{\uichex considers both \texttt{Activity.setContentView()} and \texttt{LayoutInflater.inflate()}
as the methods to load UI layouts due to their prevalence.}
along the program paths that lead to the invocation of \texttt{findViewById}.
We next describe how \uichex performs context-sensitive analysis to distinguish widget IDs between layout files.
For the description below, we use \texttt{setContentView()} as an example API.

Given a widget variable,
\uichex first identifies the method call \texttt{findViewById},
and computes an inter-procedural backward slice~\cite{Horwitz:88:PLDI,Horwitz:88:Journal,Horwitz:90:TOPLAS,Horwitz:2004} 
of its receiver object, \ie the activity object.
This backward slice traces back from \texttt{findViewById},
and includes all statements that may affect the state of the activity object.
\uichex then searches the slice backward for the method call \texttt{setContentView},
and uses the argument of the first found \texttt{setContentView} as the layout ID.
For example, in Figure~\ref{SUPOR:fig:code_motivating},
the widget variable \texttt{txtUid} is defined by the \texttt{findViewById} at Line 7,
and the activity object of this method call is an instance of \texttt{LoginActivity}.
From the backward slice of the activity object,
the first method call \texttt{setContentView} is found at Line 6,
and thus its argument {\tt R.layout.login\_activity} is associated with \texttt{txtUid},
whose widget ID is specified by \texttt{R.id.uid}.
Both \texttt{R.layout.login\_activity} and \texttt{R.id.uid}
can be further resolved to identify their numeric IDs,
and match with the contextual IDs of sensitive input fields to determine
whether \texttt{txtUid} is a widget variable for a sensitive input field.

%If so, any subsequent method call \texttt{getText()} on \texttt{txtPwd1} is considered as a taint source.

\eat{
Note that in the backward slice,
there may be multiple program paths that load different layouts, such as the following example:

{\small
\begin{verbatim}
  if(input)
     setContentView(Layout_ID1);
  else
     setContentView(Layout_ID2);
  widget = (EditText)findViewById(Widget_ID3);
\end{verbatim}
}
\noindent In this case, \uichex makes a conservative decision
and considers the widget bound to a sensitive input field
if either the input field identified by the contextual ID \texttt{<Layout\_ID1,Widget\_ID3>}
or the input field identified by the contextual ID \texttt{<Layout\_ID2,Widget\_ID3>} contains sensitive information.
}

\eat{
\subsection{Example Application: Taint Analysis}
With the identified widget variables,
various privacy analysis approaches can be applied to analyze sensitive user inputs.
As an example application, with static taint analysis~\cite{enck2010taintdroid,Long:12,flowdroid},
one can track the privacy disclosures of sensitive user input to different sinks.
To identify the sinks in the code,
\uichex leverages a pre-defined sink dataset collected based on previous work~\cite{Long:12,flowdroid}
and finds the sink locations in the code based on the APIs specified in the sink dataset.
With both the source and sink locations, \uichex
performs a taint analysis to detect leakage flows of the sensitive input fields.
Note that \uichex leverages existing static taint analysis~\cite{Long:12,flowdroid} to compute the information flows,
since static taint analysis achieves better coverage and scalability than dynamic taint analysis~\cite{enck2010taintdroid}.
}

\subsection{Keyword Dataset Construction} \label{SUPOR:subsec:design:keyword}
To collect the sensitive keyword dataset,
we crawl all texts in the resource files from 54,371 apps, including layout files and string resource files.
We split the collected texts based on newline character (\texttt{$\backslash$n}) to form a list of texts,
and extract words from the texts to form a list of words.
Both of these lists are then sorted based on the frequencies of text lines and words, respectively.
We then systematically inspect these two lists with the help of the adapted NLP techniques.
Next we describe how we identify sensitive keywords in detail.
	
First, we adapt NLP techniques to extract nouns and noun phrases from the top 5,000 frequent text lines.
Our technique first uses Stanford parser~\cite{SNLP1} to parse each text line into a syntactic tree as discussed in Section~\ref{SUPOR:subsec:nlp},
and then traverses the parse tree level by level to identify nouns and noun phrases.
For the text lines that do not contain any noun or noun phrase, our technique filters out these text lines,
since such text lines usually consist of only prepositions (\eg to), verbs (\eg update please), or unrecognized symbols.
From the top 5,000 frequent text lines, our technique extracts 4,795 nouns and noun phrases.
For the list of words, our technique filters out words that are not nouns due to the similar reasons.
From the top 5,000 frequent words, our technique obtains 3,624 words.
We then manually inspect these two sets of frequent nouns and noun phrases to identify sensitive keywords.
As phrases other than noun phrases may indicate sensitive information,
we further extract consecutive phrases consisting of two and three words from the text lists
and manually inspect the top 200 frequent two-word and three-word phrases to expand our sensitive keyword set.

Second, we expand the keyword set by searching the list of text lines and the list of words using the identified words.
For example,
we further find ``cvv code'' for credit card by searching the lists using the top-ranked word ``code'',
and find ``national ID'' by searching the lists using the top-ranked word ``id''.
We also expand the keywords using synonyms of the keywords based on WordNet~\cite{wordNet,Miller:1995:WLD:219717.219748,web:wordnet}.

Third, we further expand the keywords by using Google Translate
to translate the keywords from English into other languages.
Currently we support Chinese and Korean besides English.

These keywords are manually classified into 10 categories,
and part of the keyword dataset is presented in Table~\ref{SUPOR:table:keywords}.
Note that we do not use ``Address'' for the category ``Personal Info''.
Although personal address is sensitive information,
our preliminary results show that
this keyword also matches URL address bars in browsers, causing many false positives.
Also, we do not find interesting privacy disclosures based on this keyword in our preliminary results,
and thus ``Address'' is not used in our keyword dataset.
Although this keyword dataset is not a complete dataset that covers every sensitive keyword appearing in Android apps,
our evaluation results (in Section~\ref{SUPOR:sec:eval}) show that it is a relatively complete dataset for the ten categories that we focus on in this work.


\begin{table}[t]
	\centering
	\caption{Part of keyword dataset.} \label{SUPOR:table:keywords}%\small
	\begin{tabular}{|l||l|}
		\hline
		Category & Keywords \\ \hline \hline
	  Credential & pin code, pin number, password \\ \hline
		Health   & weight, height, blood type, calories \\ \hline
		Identity & username, user ID, nickname \\ \hline
	  Credit Card & credit card number, cvv code \\ \hline
		   SSN   & social security number, national ID \\ \hline
   Personal Info & first name, last name, gender, birthday \\ \hline
  Financial Info & deposit amount, income, payment \\ \hline
			  Contact  & phone number, e-mail, email, gmail \\ \hline
		   Account  & log in, sign in, register \\ \hline
		 Protection & security answer, identification code \\ \hline
	\end{tabular}
	\vspace{0.2in}
\end{table}



