\section{Implementation} \label{SUPOR:sec:impl}
In this section, we provide the details of our implementation of \uichex,
including the frameworks and tools we built upon and
certain tradeoffs we make to improve the effectiveness.

\uichex accepts APK files as inputs,
and uses a tool built on top of Apktool \cite{apktool}
to extract resource files and bytecode from the APK file.
The Dalvik bytecode is translated
into an intermediate representation (IR), which is based on dexlib
in Baksmali \cite{baksmali}. The IR is further converted to WALA \cite{Wala:12}
static single assignment format (SSA). WALA \cite{Wala:12} works as the
underlying analysis engine of \uichex, providing various functionalities,
\eg call graph building, dependency graph building, and point-to analysis.


The UI rendering engine is built on
the UI rendering engine from the ADT Eclipse plug-ins
%top of a sample rendering project
%provided in the SDK source code of Android 4.2~\cite{SampleRender}.
Besides improving the engine to better render custom widgets,
we also make the rendering more resilient using all available themes.
Due to SDK version compatibility, not every layout can be rendered in
every theme. We try multiple themes until we find a successful rendering.
%we also enable the engine to use multiple themes.
%In many cases, the default theme provided by a specific SDK version is insufficient
%to render all apps. Newer versions of SDK provide additional themes that
%are incompatible with older versions.
%Even the default theme in an app may not be enough for all of the layout files in that app,
%since some layouts requires different themes.
%Our solution is to create a list of themes for an app,
%and this list consists of one standard theme and the custom themes in the app.
%When rendering a layout, a theme in the list is applied.
%If the rendering fails,
%we use next available theme until the rendering succeeds.
%Moreover, we provide multiple versions of Android SDKs and try one by one until we obtain a successful rendering.
Although different themes might make UI slightly different,
%Although rendering the UI in different themes may make the UI look differently,
the effectiveness of our algorithm should not be affected.
The reason is that apps should not confuse users in the successfully rendered
themes, and thus our algorithm designed to mimic what users see the UIs should work accordingly.
%that no matter which theme is used to successfully render the app,
%the rendered UI should not confuse users.
%As our UI sensitiveness analysis algorithm mimics how users look at the UI,
%our algorithm should obtain good results for any theme that can be used to successfully render the UI.


To demonstrate the usefulness of \uichex,
we implement a privacy disclosure detection system by combining \uichex with static taint analysis.
This system enables us to conduct a study on the disclosures of sensitive user inputs.
We build a taint analysis engine on top of Dalysis~\cite{Long:12} %  similar to previous static taint analysis work~\cite{Long:12,flowdroid}
and make several customizations to improve the effectiveness.


The taint analysis engine constraints the taint propagation to only variables and method-call returns of {\tt String} type.
Therefore, method calls that return primitive types (\eg {\tt int}) are ignored.
There are two major reasons for making this tradeoff.
The first is that the sensitive information categories we focus on are passwords, user names, emails, and so on,
and these are usually not numeric values.
The second is that empirically we found a quite number of false positives related to flows of primitive types
due to the incompleteness of API models for the Android framework.
This observation-based refinement suppresses many false positives.
For example, one false warning we observed is that the length of a tainted string (\texttt{tainted.length()}) is logged,
and tracking such length causes too many false positives afterwards.
Since such flow does not disclose significant information of the user inputs,
removing the tracking of such primitive values reduces the sources to track and improves the precision of the tracking.

To further suppress false warnings,
we model data structures of key-value pairs, such as {\tt Bundle} and \texttt{BasicNameValuePair}.
{\tt Bundle} is widely used for storing an activity's previously frozen state,
and \texttt{BasicNameValuePair} is usually used to encode name-value pairs for HTTP URL parameters
or other web transmission parameters, such as JSON.
For each detected disclosure flow, we record the keys
when the analysis finds method calls that insert values into the data structures,
\eg {\tt bundle.put("key1", tai}-{\tt nted)}.
For any subsequent method call that retrieves values from the data structures, \eg {\tt bundle.get("key2")},
we compare the key for retrieving values \texttt{key2} with the recorded keys.
If no matches are found, we filter out the disclosure flow.


%This tradeoff obviously suppresses many false warnings.
%For example, a taint source is inserted into a database and then the database handle is tainted.
%Later, some index of the database is obtained via the handle.
%If we do not apply our tradeoff approach, the index is tainted and if it is leaked, we have a false warning.


To identify sensitive user inputs,
\uichex includes totally 11 source categories, including the 10 categories listed
in Section~\ref{SUPOR:subsec:design:keyword} and an additional category {\it PwdLike} for
the input fields identified as sensitive using their attributes such as {\tt inputType}.
The {\it PwdLike} category is prioritized if it has some overlapping with the other categories.
Once the widget variables of the sensitive input fields are found,
we consider any subsequent method calls on the variables that retrieve values from the input fields as source locations,
such as \texttt{getText()}.
To identify privacy disclosures of the sensitive user inputs,
\uichex mainly focuses on the information flows
that transfer the sensitive data to the following two types of sinks:
(1) the sinks of output channels that
send the information out from the phone (\eg SMS and Network)
and (2) the sinks of public places on the phone (\eg logging and content provider writes).

In details, the sink dataset includes five categories of sink APIs,
among which two categories are SMS send (\eg  {\tt SmsManager.sendTextMessage()})
and Network (\eg {\tt Ht}-{\tt tpClient.execute()}).
The other three are related to local storage: \eat{accounting for 236 APIs.
These three categories are} logging (\eg  {\tt Log}-{\tt .d()}),
content provider writes (\eg {\tt ContentResolver.insert()}),
and local file writes (\eg {\tt OutputStream.write()}).
Totally there are 236 APIs.

Our implementation, excluding the underlying libraries and the core taint analysis
engine, accounts for about 4K source lines of code (SLoC) in Java.


