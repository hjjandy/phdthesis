\section{Evaluations and Experiments} \label{SUPOR:sec:eval}
We conducted comprehensive evaluations on \uichex
over a large number of apps downloaded from the official Google Play store.
We first evaluated the performance of \uichex and demonstrated its scalability.
We then measured the accuracy of the UI sensitiveness analysis
and the accuracy of \uichex in detecting disclosures of sensitive user inputs.
In addition, our case studies on selected apps present practical insights of %SUI
sensitive user input disclosures,
which are expected to contribute to a community awareness.


\subsection{Evaluation Setup}
The evaluations of \uichex were conducted on a cluster of eight servers
with an Intel Xeon CPU E5-1650 and 64/128GB of RAM.
During the evaluations,
we launched concurrent \uichex instances on 64-bit JVM with a maximum heap space of 16GB.
On each server 3 apps were concurrently analyzed,
so the cluster handled 24 apps in parallel.

In our evaluations, we used the apps collected from the official Google Play store in June 2013.
We applied \uichex to analyze 6,000 apps ranked by top downloads,
with 200 apps for each category.
Based on the results of the 6,000 apps,
we further applied \uichex on another 10,000 apps in 20 selected categories.
Each of the 20 categories is found to have at least two apps with %SUI
sensitive user input disclosures.

For each app, if it contains at least one input field in layout files,
the app is analyzed by the UI sensitiveness analysis.
If \uichex identifies any sensitive input field of the app,
the app is further analyzed by the taint analysis to detect %SUI
sensitive user input disclosures.
Table~\ref{SUPOR:table:stat16000apps} shows the statistics of these apps.
A small portion of the apps do not contain any layout files
and about 1/3 of the apps do not have any input field in layout files.
This is reasonable because many Game apps do not require users to enter information.
35\% of the apps without layout files and 17\% of the apps without
input fields belong to different sub-categories of games.
11 apps (0.07\%) cannot be analyzed by \uichex due to various parsing errors
in rendering their layout files.
In total, 60.33\% of the apps contain input fields in their layout files,
among which more than half of the apps are further analyzed
because sensitive input fields are found via the UI sensitiveness analysis.

\begin{table}[t]
	\centering
	\caption{Statistics of 16,000 apps.} \label{SUPOR:table:stat16000apps}%\small
	\begin{tabular}{| l || r | r |}
		\hline
		                          &  \#Apps   & Percentage \\ \hline \hline
          Without Layout Files & 625       &  3.91\%    \\ \hline
         Without Input Fields  & 5,711     & 35.69\%    \\ \hline
Without Sensitive Input Fields & 4,731     & 29.57\%    \\ \hline
  With Sensitive Input Fields  & 4,922     & 30.76\%    \\ \hline
               Parsing Errors & 11        &  0.07\%    \\ \hline \hline
                     TOTAL    & 16,000    & 100.00\%   \\ \hline
	\end{tabular}
	\vspace{0.2in}
\end{table}

As not every layout containing input fields is identified with sensitive input fields,
we show the statistics of the layouts for the 4,922 apps identified with sensitive input fields.
Among these apps, 47,885 layouts contain input fields
and thus these layouts are rendered.
Among the rendered layouts,
19,265 (40.2\%) are found to contain sensitive keywords
(no matter whether the keywords are associated with any input field).
This is the upper bound of the number of layouts that can
be identified with sensitive input fields.
In fact, 17,332 (90.0\%) of the 19,265 layouts with sensitive keywords
are identified with sensitive input fields.

\subsection{Performance Evaluation}
The whole experiment for 16,000 apps takes 1439.8 minutes,
making a throughput of 11.1 apps per minutes on the eight-server cluster.
The following analysis is only for the 4,922 apps identified with sensitive input fields, if not specified.

The UI analysis in \uichex includes decompiling APK files,
rendering layouts, and performing UI sensitiveness analysis.
For each app with sensitive input fields,
\uichex needs to perform the UI analysis for at least 1 layout and at most 190 layouts,
while the median number is 7 and the average number is 9.7.
%The execution time of the UI analysis is also presented in Figure~\ref{SUPOR:fig:6000performance}.
Though the largest execution time required for this analysis is about 2 minutes.
96.3\% of the apps require less than 10 seconds to render all layouts in an app.
The median analysis time is 5.2 seconds and the average time is 5.7 seconds for one app.
Compared with the other parts of \uichex,
the UI analysis is quite efficient, accounting for only 2.5\% of the total analysis time on average.
Also, the UI sensitiveness analysis,
including the correlation score computation and keyword matching,
accounts for less than 1\% of the total UI analysis time,
while decompiling APK files and rendering layouts take most of the time.

To detect sensitive user input disclosures,
our evaluation sets a maximum analysis time of 20 minutes.
18.1\% of the apps time out in our experiments but 73.7\% require less than 10 minutes.
The apps with many entry points tend to get stuck in taint analysis, and are more likely to timeout.
Scalability of static taint analysis is a hard problem, but we are not worse than related work.
The timeout mechanism is enforced for the whole analysis,
but the system will wait for I/O to get partial results. 	
In practice, we can allow a larger maximum analysis time so that more apps can be analyzed.
Among the apps finished in time, the median analysis time is 1.9 minutes and
the average analysis time is 3.7 minutes.

The performance results show that
\uichex is a scalable solution that can statically analyze UIs of a massive number of apps
and detect %SUI
sensitive user input disclosures on these apps.
Compared with existing static taint analysis techniques,
the static UI analysis introduced in this work is highly efficient,
and its performance overhead is negligible.

\subsection{Effectiveness of UI Sensitiveness Analysis}
To evaluate the accuracy of the UI sensitiveness analysis,
we randomly select 40 apps
and manually inspect the UIs of these 40 apps to
measure the accuracy of the UI sensitiveness analysis.

First, we randomly select 20 apps reported {\it without} sensitive input fields,
and manually inspect these apps to measure the false negatives of \uichex.
In these apps, the largest number of layouts \uichex
renders is 5 and the total number of layouts containing input fields is 39 (1.95 layouts per app).
\uichex successfully renders 38 layouts and identifies 57 input fields (2.85 input fields per app).
\uichex fails to render 1 layout due to the lack of necessary themes for a third-party library.
By analyzing these 57 input fields, we confirm that \uichex has only one false negative (FN),
\ie failing to mark one input field as sensitive in the app {\it com.strlabs.appdietas}.
This input field requests users to enter their weights, belonging to the Health category in our keyword dataset.
However, the text of the descriptive text label for the input field is ``Peso de hoy'',
which is ``Today Weight'' in Spanish.
Since our keyword dataset focuses on sensitive keywords in English, \uichex has a false negative.
Such false negatives can be reduced by expanding our keyword dataset to support more languages.

\eat{Among these 20 apps, 4 apps contain custom components.
\uichex renders 10 custom input fields in these 4 apps and 2 custom text labels in 1 app.
We manually analyze the rendered UIs to compute the accuracy of the UI analysis.}



\eat{
\uichex applies all available themes but still cannot obtain some style references,
leading to a missing resource problem that the render engine cannot handle.
This layout belongs to a third-party library introduced in the app {\it com.neomit.xxx}.
}



\begin{table*}[tp]
	\centering
	\caption{UI analysis details for 20 randomly chosen apps.} \label{SUPOR:table:ui_accuracy_sensitive}\small
	\begin{tabular}{| l || c|c|c|c|c|c|c|c|c| l |}
		\hline
App    &    \#Layouts    &    \#Input    &    \#Layouts       &\multicolumn{4}{c|}{\#Reported SIF}&  FP & FN &  Dup  \\
\hhline{~~~~----~}
ID &                 &               Fields       & with SIF &  Password &  Hint & Label & Total     &       & &    ID     \\ \hline \hline
1    &    8    &    18    &    4    &    6    &    0    &    3    &    9    &   & 2  &  $\circ$     \\ \hline  % no bullet here
2    &    37    &    77    &    2    &    0    &    0    &    8    &    8    &    & &        \\ \hline
3    &    3    &    3    &    1    &    0    &    1    &    0    &    1    &    & &        \\ \hline
4    &    4    &    9    &    3    &    0    &    0    &    6    &    6    &     & &  $\circ$        \\ \hline
5    &    5    &    7    &    1    &    1    &    0    &    0    &    1    &      & &         \\ \hline
6    &    17    &    52    &    10    &    6    &    12    &    12    &    30    & 1    & &  $\circ$        \\ \hline
7    &    4    &    5    &    2    &    0    &    0    &    3    &    3    &        & &       \\ \hline
8    &    15    &    22    &    9    &    8    &    3    &    2    &    13    &    1  & &         \\ \hline
9    &    3    &    7    &    1    &    1    &    1    &    0    &    2    &       & &        \\ \hline
10    &    7    &    16    &    1    &    0    &    0    &    1    &    1    &      & &         \\ \hline
11    &    5    &    6    &    1    &    1    &    1    &    0    &    2    &        & &       \\ \hline
12    &    17    &    33    &    8    &    8    &    9    &    0    &    17    &    & &   $\circ$ $\bullet$        \\ \hline
13    &    26    &    60    &    10    &    0    &    0    &    12    &    12    &   & &  $\circ$ $\bullet$        \\ \hline
14    &    2    &    8    &    2    &    1    &    0    &    4    &    5    &  2    & 1&         \\ \hline
15    &    14    &    26    &    5    &    2    &    3    &    0    &    5    &     & &  $\circ$ $\bullet$        \\ \hline
16    &    4    &    7    &    1    &    1    &    0    &    0    &    1    &     & &          \\ \hline
17    &    4    &    8    &    3    &    2    &    3    &    0    &    5    &     & &  $\bullet$        \\ \hline
18    &    29    &    25    &    4    &    4    &    0    &    6    &    10    &     & &  $\circ$        \\ \hline
19    &    24    &    37    &    8    &    9    &    6    &    1    &    16    &     & &  $\circ$        \\ \hline
20    &    1    &    2    &    1    &    0    &    2    &    0    &    2    &        & &       \\ \hline \hline
Total    &    229    &    428    &    77    &    50    &    41    &    58    &    149    &   4   & 3 &         \\ \hline
	\end{tabular}
    \panictwo{5pt}
\end{table*}


Second, we randomly select 20 apps reported {\it with} sensitive input fields.
Table~\ref{SUPOR:table:ui_accuracy_sensitive} shows the detailed analysis results.
Column ``\#Layouts '' counts the number of layouts containing input fields in each app,
while Column ``\#Layouts with SIF'' presents the number of layouts reported with sensitive input fields.
Column ``\#Input Fields'' lists the total number of input fields in each app
and Column ``\#Reported SIF'' gives
the detailed information about how many input fields are identified by checking the {\tt inputType}
attribute, by matching the hint text, and by analyzing the associated text labels.
Sub-Column ``Total'' presents the total number of sensitive input fields identified by \uichex in each app.
Columns ``FP'' and ``FN'' show the number of false positives and the number of false negatives
produced by \uichex in classifying input fields.
Column ``Dup ID'' shows if an app contains any duplicate widget ID for sensitive input fields.
These duplicate IDs belong to either sensitive input fields (represented by $\circ$) or non-sensitive input fields ($\bullet$).
For all the layouts in these 20 apps,
\uichex successfully renders the layouts except for App 18,
which has 29 layouts containing input fields but \uichex renders only 17 layouts.
The reason is that Apktool fails to decompile the app completely.

The results show that for these 20 apps, \uichex identifies 149 sensitive input fields
with 4 FPs and 3 FNs, and thus the achieved true positives (TP) is 145.
Combined with the 20 apps identified without sensitive input fields (0 FP and 1 FN),
\uichex achieves an average precision of 97.3\%
(precision = $\frac{TP}{TP + FP}$ = 145/149)
and an average recall of 97.3\% (recall = $\frac{TP}{TP + FN}$ = 145/(145+(1+3)).

We next describe the reasons for the FNs and the FPs.
\uichex has two false negatives in App 1, in which the text label ``Answer''
is not identified as a sensitive keyword.
But according to the context, it means ``security answer'', which should be sensitive.
Although this phrase is modeled as a sensitive phrase in our keyword dataset,
\uichex cannot easily associate ``Answer'' with the phrase, resulting in a false negative.
In App 8, \uichex marks an input field as sensitive because the associated text label containing the keyword ``Height''.
However, based on the context, the app actually asks the user to enter the expected page height of a PDF file.
Such issues can be alleviated by employing context-sensitive NLP analysis~\cite{wordrep-ACL12}.

\uichex also has two FPs in App 6 and App 8
due to the inaccuracy of text label association.
In App 6 shown in Figure~\ref{SUPOR:fig:ui_accuracy_FP_case},
the hint of the ``Delivery Instructions'' input field does not contain sensitive keywords,
and thus \uichex identifies the close text label for determining its sensitiveness.
However, \uichex incorrectly associates a description label of ``Email'' to the ``Delivery Instructions'' input field
based on their close distances.
Since this description contains sensitive keywords such as email,
\uichex considers the ``Delivery Instructions'' input field as sensitive,
causing a false positive.
Finally, \uichex has both FPs and FNs for App 14,
since its arrangements of input fields and their text labels are not accurately
captured by our position-based weights that give preferences for left and top positioned text labels.

\begin{figure}[t]
	\centering
	\includegraphics[clip,width=0.95\textwidth]{figs/ui_accuracy_FP_case.eps}
	\caption{False positive example in UI sensitiveness analysis.} \label{SUPOR:fig:ui_accuracy_FP_case}

\end{figure}

\eat{In one layout which vertically aligns 6 input fields,
4 of which should be identified as sensitive.
\uichex reports 3 but our manually analysis finds that 2 are false positives.
There false positives are caused by the position-based weights.
Even if the input field for one false positive is also sensitive, \uichex matches a
wrong text label to it.
In this layout, the text labels overlap the left part of corresponding
input field, making that text label \texttt{txt_i} is at the near top of input field
B if input field A is at top of B. But text label B is inside input field B, such that the position-based weight
for label B is greater than that for label A. Finally, field B is associated with label A.
Therefore, only the first reported field is correct.}

To evaluate the effectiveness of resolving duplicate IDs,
We instrumented \uichex to output detailed information when identifying the widget variables.
We did not find any case where \uichex incorrectly associates the widget variables
with the input fields based on the contextual IDs,
but potentially \uichex may have inaccurate results
due to infeasible sequences of entry points that can be executed.
We next present an example to show how backward slicing help \uichex distinguish duplicate widget IDs.
App 17 has two layouts with the same hierarchy.
Layout A contains a sensitive input field with the ID $w1$ while Layout B
contains a non-sensitive input field with the same ID $w1$.
Both layouts are loaded via {\tt LayoutInflater.inflate}
and then {\tt findViewById} is invoked separately to obtain the enclosed input fields.
Without the backward slicing, \uichex considers the input field with the ID $w1$ in the Layout B as sensitive,
which is a false positive.
With the backward slicing,
\uichex can distinguish the input field with the ID $w1$ in Layout B
with the input field with the ID $w1$ in Layout A,
and correctly filter out the non-sensitive input field in Layout B.

\subsection{Accuracy of Detecting Sensitive User Input Disclosures}
%355 (2.2\%) out of the 16,000 apps are reported with %SUI
In our experiments, 355 apps are reported with sensitive user input disclosures.
The reported apps belong to 25 out of the 30 categories in Google Play Store
and 20 categories have at least 2 apps reported.
We next report the accuracy of detecting sensitive user input disclosures.
%The most vulnerable categories
%are SHOPPING (10), TRANSPORTATION (8), BUSINESS (8), HEALTH \&\& FITNESS (7),
%and LIFESTYLE (7). Totally 40 apps (38.8\%) are reported for these 5 categories.

%Table~\ref{SUPOR:table:6000report_bycategory} shows the breakdown results of the detected UI leaks by category.
\eat{The reported apps come from 21 out of the 30 categories in Google Play Store.
The number of reported apps for each category is shown in 2\nd column.
For each reported app, we manually inspect the app to confirm
whether the identified %SUI
sensitive user input disclosures are false warnings.
The 3\rd column shows the number of false positives produced by \uichex. }

Figure~\ref{SUPOR:fig:6000_by_src_sink} shows the number of true positives and
the number of false positives by taint source and sink categories.
If an app is reported with multiple disclosure flows and one of them is a false positive,
the app is considered as a false positive.
Through manually evaluating the 104 apps reported cases from the first 6,000 analyzed apps,
we find false positives in 9 apps.
Therefore, the overall false positive rate is about 8.7\%, \ie the accuracy of privacy disclosure detection is 91.3\%.
We investigated the false positives and found that
these false positives were mostly resulted from
the limitations of the underlying taint analysis framework,
such as the lack of accurate modeling of arrays.

\eat{
In our evaluations, \uichex uses 11 source categories,
10 for sensitive keyword categories (Section~\ref{SUPOR:subsec:design:keyword})
and 1 specific category for input fields that can be directly identified
via the {\tt inputType} attribute.
Among the 11 source categories, \uichex reports UI leaks for 9 categories,
and the categories of {\it PwdLike} and {\it Contact} have the largest
number of reported apps as well as true positives
(see Figure~\ref{SUPOR:fig:6000_by_src_sink}\subref{SUPOR:subfig:6000_by_src}).
Among the 9 categories that UI leaks are reported,
false positives are found in 5 categories. \eat{, with one for each of these 4 categories.}

Figure~\ref{SUPOR:fig:6000_by_src_sink}\subref{SUPOR:subfig:6000_by_sink}
shows the breakdown results of UI leaks by sink categories.
If an app is reported with UI leaks of multiple sink categories,
the app is counted for each different sink category that the UI leaks flow to.
\uichex uses 5 sink categories, as discussed in Section~\ref{SUPOR:sec:impl}.
The results show that \uichex reports UI leaks for 4 of these 5 sink categories.
and most UI leaks belong to the network and logging categories.}

\eat{
We manually check the minimum SDK version required by the apps,
all of them have a value smaller than
16 (usually 8), after which third-party apps cannot access other apps' log.
Such finding indicates that app developers aim at running their apps
in smartphones with older versions of Android.
In such versions, malware can easily retrieve the log
and obtain the sensitive information leaked to log.
Moreover, due to the small minimum SDK version, the content providers are made publically accessed by
other apps by default.
But the apps reported with leakage flows to the content provider
do not protect the content provider via permissions or setting \texttt{android:export=false}.
Thus, malware can easily access the leaked sensitive information via reading from the content provider.}

\begin{figure}[tp]
	\centering
	\subfloat[][TPs and FPs by source categories.] {
		\includegraphics[angle=-90,width=.98\textwidth,clip]{figs/6000report_by_src.eps}
		\label{SUPOR:subfig:6000_by_src}
	}

	\subfloat[][TPs and FPs by sink categories.] {
		\includegraphics[angle=-90,width=.98\textwidth,clip]{figs/6000report_by_sink.eps}
		\label{SUPOR:subfig:6000_by_sink}
	}

	\caption{\linespread{1}\selectfont True positives and false positives by source/sink categories
		for the reported apps.}
		\label{SUPOR:fig:6000_by_src_sink}

\end{figure}



\eat{
We instrument \uichex to output detailed information to evaluate the effectiveness of solving
duplicate ID problem. We find that \uichex can successfully eliminate the non-sensitive IDs,
\ie not associating the widget variables in the code to the duplicate non-sensitive input fields
in the apps marked with $\bullet$ in Table~\ref{SUPOR:table:ui_accuracy_sensitive}.
For example, App 17 has two layouts
with the same hierarchy, but only one (layout A) contains sensitive input fields while the other one (layout B)
contains fields with the same IDs as in A. Both layouts are loaded via {\tt LayoutInflater.inflate}
at different program locations and then {\tt findViewById} is invoked to obtain the enclosed
input fields. Before applying backward slicing, \uichex reports
a leakage flow from the fields in B to logging. But with the slice, \uichex discovers that
the $<$layout ID\_B, widget ID$>$ pair does not indicate a sensitive input field. Therefore,
\uichex does not track any subsequent method calls for the widget variable associated with
the input field in B.}

\eat{
For the 10000-experiment, \uichex identifies 251 apps (2.5\% of the detected apps)
in 20 categories to have UI leaks.
Such result indicates that many apps in the official Google Play Store
suffer from UI leaks,
since the developers are still not aware of the risks.
Among the 20 categories,
5 categories (BUSINESS (35), SHOPPING (27), COMMUNICATION (24), SOCIAL (22) and TRAVEL \& LOCAL (17) \eat{FINANCE (15)}
are found to have more than \eat{10} 15 apps with UI leaks and account for \eat{59.3\%} 49.8\% of the reported apps.
Apps of these categories usually require users to register
accounts with their personal information,
and apps of the BUSINESS and SHOPPING categories also require
users' financial information (\eg credit card information) to provide their services.
Thus, apps from these categories tend to have standard input fields,
and are more vulnerable to UI leaks.
}

\eat{ % whole sec moved
\subsection{Large-Scale Study}
In our large-scale study,
we select more apps from the app categories reported to have at least two apps having UI leaks
%based on the results shown in Table~\ref{SUPOR:table:6000report_bycategory}.
in the 6000-app experiment.
Apps of certain categories may have few layouts containing input fields, such as Game apps,
and thus are less interesting for our investigation.
Based on the results of our 6000-app experiment, \eat{shown in Table~\ref{SUPOR:table:6000report_bycategory},}
we select 500 apps from 20 categories, totally 10,000 apps, for our large-scale study.
This experiment takes 750.3 minutes to finish and the throughput is 13.3 apps per minute
on the eight-server cluster.

%Table~\ref{SUPOR:table:8500report} shows the results of the 8,500 apps for our large-scale study.
The result shows that \uichex identifies 251 apps (2.5\% of the detected apps)
in 20 categories to have UI leaks.
Such result indicates that many apps in the official Google Play Store
suffer from UI leaks,
since the developers are still not aware of the risks.
Among the 20 categories,
5 categories (BUSINESS (35), SHOPPING (27), COMMUNICATION (24), SOCIAL (22) and TRAVEL \& LOCAL (17) \eat{FINANCE (15)}
are found to have more than \eat{10} 15 apps with UI leaks and account for \eat{59.3\%} 49.8\% of the reported apps.
Apps of these categories usually require users to register
accounts with their personal information,
and apps of the BUSINESS and SHOPPING categories also require
users' financial information (\eg credit card information) to provide their services.
Thus, apps from these categories tend to have standard input fields,
and are more vulnerable to UI leaks.

\eat{Apps of the COMMUNICATION and SOCIAL categories require users to register
accounts with their personal information,
and apps of the BUSINESS, FINANCE, and SHOPPING categories also require
users' financial information (\eg credit card information) to provide their services.
Thus, apps from these categories tend to have standard input fields,
and are more vulnerable to UI leaks.}
\eat{Category PHOTOGRAPHY, which has 2 apps reported in our 6000-app experiment, does not have any apps reported.}
\eat{
\begin{table}[t]
	\centering
	\caption{Detection results by category for 8,500 apps.}
	\label{SUPOR:table:8500report}
	\begin{tabular}{| l | c |}
		\hline
		Category         &   \#Apps Reported   \\ \hline \hline
BOOKS \&\& REFERENCE	& 5 \\ \hline
BUSINESS	& 22 \\ \hline
CARDS	& 1 \\ \hline
COMMUNICATION	& 21 \\ \hline
ENTERTAINMENT	& 3 \\ \hline
FINANCE	& 11 \\ \hline
HEALTH \&\& FITNESS	 & 6 \\ \hline
LIFESTYLE	& 8 \\ \hline
MEDIA \&\& VIDEO	& 3 \\ \hline
MEDICAL	& 5 \\ \hline
NEWS \&\& MAGAZINES	& 3 \\ \hline
SHOPPING	& 13 \\ \hline
SOCIAL	& 13 \\ \hline
TOOLS	& 4 \\ \hline
TRANSPORTATION	& 8 \\ \hline
TRAVEL \&\& LOCAL	& 9 \\ \hline \hline
TOTAL  &  135 \\ \hline
	\end{tabular}
\end{table}
}
}

\subsection{Case Studies}
\label{SUPOR:subsec:case}
To improve the community's awareness and understanding of %SUI
sensitive user input disclosures,
we conducted cases studies on four selected apps from the source categories
SSN, PwdLike, Credit Card, and Health.
These case studies present interesting facts of %SUI
sensitive user input disclosures,
and also demonstrate the usefulness of \uichex.
%We hide part of the app package names as a precaution to not leak undisclosed vulnerability information.
We also inform the developers of the apps mentioned in this section about the detected disclosures.

{\it com.yes123.mobile} is an app for job hunting.
The users are required to register with their national ID
and a password to use the service.
When the users input the ID and password, and then click log in (see Figure~\ref{SUPOR:fig:casestudy_loginyes123}),
the app sends both their national IDs and passwords via Internet without any protection
(\eg hashing or through HTTPS channel).
Since national ID is quite sensitive (similar as Social Security Number),
such limited protection in transmission may lead to serious privacy disclosure problems.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.85\textwidth,clip]{figs/casestudy_loginyes123.eps}
	\caption{Case study: National ID and password disclosure example without protection.}
	\label{SUPOR:fig:casestudy_loginyes123}
\end{figure}

The second example app ({\it craigs.pro.plus})
shows a legitimate disclosure where %that
%uses HTTPS connections to send user sensitive inputs to its server for authentication.
HTTPS connections are used to send user sensitive inputs to its server for authentication.
%For example, HTTPS is used to transmit password to its server for authentication.
Even though the password itself is not encoded (\eg hashing),
we believe HTTPS connections provide a better protection layer to resist the disclosures during communications.
Also we find that popular apps developed by enterprise companies are more likely to adopt HTTPS,
providing better protection for their users.

To better understand whether %SUI
sensitive user inputs are properly protected,
we further inspect 104 apps,
of which 44 apps send %SUI
sensitive user inputs via network.
Among these 44 apps, only 10 of them adopt HTTPS connections,
while the majority of apps transmit %SUI
sensitive user inputs in plain text via HTTP connections.
Such study results indicate that most developers are still unaware of the risks posed by
sensitive user input disclosures, and more efforts should be devoted to provide more
protections on sensitive user inputs.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.85\textwidth, clip]{figs/casestudy_registration.eps}
	\caption{Case study: Credit card information disclosure example.}
	\label{SUPOR:fig:casestudy_creditcardinfo}
\end{figure}

Our third example app ({\it com.nitrogen.android}) discloses
credit card information, a critical financial information provided by the users.
Figure~\ref{SUPOR:fig:casestudy_creditcardinfo} shows the rendered UI of the app.
The three input fields record credit card number, credit card security number,
and the card holder's name.
Because these fields are not decorated with \texttt{textPassword} input type and
they do not contain any hints,
\uichex uses the UI sensitiveness analysis to compute correlation scores for each text label.
As we can see from the UI,
the text label ``Credit Card Number'' and the text label ``Credit Card Security Number''
are equally close to the first input field.
As our algorithm considers weights based on the relative positions between text labels and input fields,
\uichex correctly associates the corresponding text labels for these three input fields,
and the taint analysis identifies %SUI
sensitive user input disclosures for all these three input fields to logging.

Our last example shows that \uichex also identifies apps that disclose personal health information to logging.
Figure~\ref{SUPOR:fig:casestudy_health} shows the rendered UI of the layout {\it dpacacl}
in app {\it com.canofsleep.wwdiary}, which belongs to the category HEALTH \&\& FITNESS.
This app discloses personal health information through the user inputs collected from the UI.
As we can see, even though all input fields on the UI hold hint texts,
these texts do not contain any sensitive keywords.
Therefore, \uichex still needs to identify the best descriptive text label for each input field.
Based on the UI sensitiveness analysis,
\uichex successfully marks the first three input fields as sensitive,
\ie the input fields that accept {\it weight}, {\it height} and {\it age}.
But based on the taint analysis, only the first two input fields are detected with disclosure flows to logging.
Similar to financial information, such health information about users' wellness is also very sensitive to the users.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.85\textwidth,clip]{figs/casestudy_dpacalc.eps}
	\caption{Case study: Health information disclosure.}
	\label{SUPOR:fig:casestudy_health}
\end{figure}


Although Google tries to get rid of some of the known sinks that contribute most of the
public leaks by releasing new Android versions,
many people globally may still continue using older Android releases for
a very long time (about 14.2\% of Android phones globally using versions older than Jelly Bean~\cite{dashboards}).
If malware accesses the logs on these devices, all the credit card information can be exploited to
malicious adversaries.
Thus, certain level of protection is necessary for older versions of apps.
Also, \uichex finds that some apps actually sanitize the sensitive user inputs (\eg hashing)
before these inputs are disclosed in public places on the phone,
indicating that a portion of developers do pay attention to protecting %SUI
sensitive user input disclosures on the phone.




\eat{ % removed by jianjun
\kangjie{Added, please add more details, e.g., field info and screenshots} \\
Libra (\verb|net.cachapa.libra|) is a weight manager app, which provides the functionality that tracks weight changes and displays changes in a chart.
\uichex detects that this app loads user inputs of contacts and sends such sensitive user inputs to a server.
Based on its functionality, we do not find any justification that requires to send users' contacts to server,
and its description also does not provide explanations for such disclosures.
By presenting such disclosures detected by \uichex, user can be aware of the risk before inputting the contacts.
}

\eat{


\begin{figure}
	\centering
	\includegraphics[width=0.35\textwidth,clip]{figs/casestudy_set_use_points_change.eps}
	\caption{Case study: Phone number transmitted without protection.}
	\label{SUPOR:fig:casestudy_set_use_points_change}
\end{figure}



The second example app, {\it com.timespread.xxx}, provides functionality to help users manage timetables and schedules.
When using this app, the user need to communicate with the service provider via Internet.
In order to authenticate the user, the provider requires the user to input the phone number
and later an authentication code is sent to that phone number (see Figure~\ref{SUPOR:fig:casestudy_set_use_points_change}).
The phone number is transmitted through Internet without any protection (\eg encryption or HTTPS).
\uichex reports this as a privacy disclosure problem.}
%The developers may not have a concern about this privacy disclosure problem, if unintentionally,
%which can cause serious problems to the app users.


\eat{
\begin{figure}
	\centering
	\includegraphics[width=0.3\textwidth,clip]{figs/casestudy_dpacalc.eps}
	\caption{Case study: Health information leak example.}
	\label{SUPOR:fig:casestudy_health}

\end{figure}


Our last example app discloses personal health information through the user inputs collected from the UI.
Figure~\ref{SUPOR:fig:casestudy_health} is the rendered UI of layout {\it dpacacl}
in app {\it com.canofsleep.xxx}, which belongs to the category HEALTH \&\& FITNESS.
As we can see, even though all input fields on the UI hold hint texts,
these texts do not contain any sensitive keywords.
Therefore, \uichex still needs to identify the best descriptive text label for each input field.
Based on the UI sensitiveness analysis,
\uichex successfully marks the first three input fields as sensitive,
\ie the input fields that accept {\it weight}, {\it height} and {\it age}.
But based on the taint analysis, only the first two input fields are detected with leakage flows to logging.
Similar to financial information, such health information about users' wellness is also very sensitive to the users.



\begin{figure}
	\centering
	\includegraphics[width=0.3\textwidth,clip]{figs/casestudy_abch.eps}
	\caption{Case study: Password disclosed via SMS.}
	\label{SUPOR:fig:casestudy_sms}
\end{figure}
 Figure~\ref{SUPOR:fig:casestudy_sms} shows a rendered UI in app {\it com.codigo4.xxx}, which is a FINANCE app.
The marked input field is attributed by {\tt inputType="textPassword"}, which can be easily recognized
as a sensitive field. \uichex  discovers that the input value is sent out via SMS through a very short
execution path in the app, without any protection on the data (\eg encryption).
}


\eat{
\begin{figure}
	\centering
	\includegraphics[width=0.3\textwidth,clip]{figs/casestudy_antitheft_sms_send.eps}
	\caption{Case study: Password leak example.}
	\label{SUPOR:fig:casestudy_credential}

\end{figure}

The first example app discloses passwords from customized input fields.
Figure~\ref{SUPOR:fig:casestudy_credential} shows the rendered UI of layout \textit{antitheft\_sms\_send}
in app \textit{com.drweb.xxx}.
The leaked input field accepts passwords entered by users, and is marked in the figure.
This input field is not decorated with a \texttt{textPassword} type attribute,
and it does not contain any hint text inside the input field.
Therefore, \uichex cannot easily determine its sensitiveness without the UI sensitiveness analysis.
As described in Algorithm~\ref{SUPOR:alg:uisensitiveness}, \uichex computes the scores for each text label
based on their distances and relative positions to the input field,
and chooses the one with the smallest score computed.
In this case, \uichex selects the closest text label which holds a description containing a sensitive keyword ``password''.
As ``password'' is one of the sensitive keywords in our keyword dataset,
\uichex correctly marks the input field as sensitive and performs a taint analysis to detect the leakage flow.
}
\eat{
\begin{figure}
	\centering
	\includegraphics[width=0.3\textwidth,clip]{figs/casestudy_mycreditcards_billing_information_for_guest.eps}
	\caption{Case study: Credit card number leak example.}
	\label{SUPOR:fig:casestudy_creditcard}
\end{figure}

Figure~\ref{SUPOR:fig:casestudy_creditcard} shows an example of credit card number leakage.
The leakage occurs in {\it com.skcc.xxx}.
When the user places an order via this app, the billing information is required to enter.
When later the order detail is shown to the user, the credit card number is written to log
for debug purpose.
}
