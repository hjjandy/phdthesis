%
%  revised  introduction.tex  2011-09-02  Mark Senn  http://engineering.purdue.edu/~mark
%  created  introduction.tex  2002-06-03  Mark Senn  http://engineering.purdue.edu/~mark
%
%  This is the introduction chapter for a simple, example thesis.
%


\chapter{INTRODUCTION}

While smartphones and mobile apps have been an essential part of 
our life, personal security issues on smartphones, including
privacy and malicious behaviors, become a serious concern. 

{\bf Privacy Issues.} 
Previous mobile privacy
related research efforts have largely focused on predefined known
sources managed by smartphones. More specifically, they focus on
the API functions that return sensitive values, such as device 
identifiers (phone number, IMEI, \etc), location, contact, 
calendar, browser state, most of which are permission protected. 
although these data sources are very important, they do not cover 
all sensitive data related to users' privacy.  

A major type of sensitive data that has been largely neglected is 
the {\it sensitive user input}, which refers to the sensitive 
information entered by users via the user interface. Many mobile 
apps today acquire sensitive credentials, financial, health, and 
medical information from users through the user interfaces. While 
all kinds of user inputs, either sensitive or insensitive are 
retrieved in the code via the same API functions, traditional 
sensitive data disclosure detection techniques that require
predefined data sources are not enough because they lack  
mechanism of deciding the sensitiveness of those API returns.
In the context of static detection of mobile apps, prior to 
performing sensitive data disclosure detection, we must resolve the
challenges of discovering the input fields from an app's user
interface, identifying which input fields are sensitive and 
associating the sensitive input fields to the corresponding 
variables in the code. 

On the other hand, existing techniques require the data 
sensitiveness of the sources to be known before the static 
detection is conducted. Even the 
above problem is related to determine the data sensitiveness 
based on where the data is produced. With the knowledge of 
sensitiveness at the source points, a forward data flow needs to be  
observed between sources and sinks in order to report a disclosure
defect. However, some generic API functions may return sensitive 
values, depending on the context, although they may return 
insensitive values in many cases, and we have no way to understand
the data sensitiveness from where the data is generated. For 
example, a local file or the network response may contain sensitive
values but given the file or the network request, we cannot 
claim the sensitiveness of the response. In such cases, most 
existing approaches would not work properly. We cannot simply 
treat the generic API functions as the sensitive data sources as
that will lead to a large number of false warnings, or just 
ignore all of such cases because we can expect missing warnings.
In addition, forward data flow analysis is insufficient. In many
cases, a piece of data may be first emitted through a sink and 
then later recognized as sensitive somehow.

{\bf Stealthy Behaviors.} 
Detecting malicious behaviors, especially stealthy behaviors, are also 
a hot research topic. Existing techniques mainly depends on 
identifying certain known fingerprints of the malicious operations
in Android apps. For example, if an app contains directly making 
phone calls or sending short messages to a known premium numbers,
or accessing a URL of known malicious website, the app will be 
reported as malware. More sophisticated cases cannot be detected.
For instance, adversaries may obfuscate the numbers or URLs in the 
code such that static analysis is not able to understand whether 
the numbers or URLs are blacklisted. Besides, maintaining a blacklist
of the numbers and URLs are not trivial. In some countries, the 
premium numbers are the same as normal phone numbers. They can also be
actively changed to avoid the blacklist detection.
Since the code behaviors, represented by API invocations, of malicious 
actions and benign operations are the same, we cannot report malicious
behaviors by only inspecting the API calls when we do not know whether 
the destinations are good or bad.


\section{Thesis Statement}

This dissertation addresses the important issue on the detection of sensitive data
disclosures in mobile apps by presenting two approaches. First, it proposes \uichex, a 
static technique that can automatically, scalably and precisely detect the data
sensitiveness of user inputs from user interfaces. Second, it introduces \bidtext, a technique 
that can recognize the sensitiveness of data generated from even more generic API functions,
such as data from network or files.
The dissertation also presents AsDroid to detect stealthy behaviors in Android apps.

The thesis statement is as follows:
Existing privacy related techniques that have mostly focused on predefined sensitive data
sources are not enough to detect sensitive data disclosures, when the data generated 
by some generic API functions (such as reading data
from user interface, files, network and so on) are neglected; 
and utilizing maliciously known destinations to detect stealthy behaviors is not sufficient.
Proposing an automated technique to identify the sensitiveness of user inputs can 
help the detectors discover the sensitive user input disclosure problems.
Introducing the bi-directional text correlation analysis can handle even more 
cases in which the data sensitiveness cannot be determined at the data generation 
points.
Combining code behavior analysis and user interface analysis can tell whether certain
program behaviors contradict the user expectation, namely, whether they are stealthy 
behaviors.


\section{Contributions}

The contributions of this dissertation are as follows:

\begin{itemize}
\item 
We propose \uichex, a static technique that can automatically, scalably and precisely determine
the data sensitiveness of user inputs in Android apps. \uichex 
achieves the following three challenging goals:
(1) it systematically discovers the input fields from an app's UI;
(2) identifies which input fields are sensitive by associating
the input fields with mostly correlated text labels and performing
text analysis to determine whether the user inputs contain sensitive
data;
(3) and associates the sensitive input fields to the corresponding 
variables in the apps that store their values.


\item 
We design and develop a novel technique, \bidtext, to statically
detect sensitive data disclosures in Android apps while the data
can be generated from more generic API functions like reading from
files or network. Since the data sensitiveness of such generic 
API functions cannot be determined from the definition locations,
we address the challenges by typing the correlated text labels 
to corresponding variables and propagating them bi-directionally
along forward and backward data-flow. The problem is formalized 
in a type system and we have obtained some preliminary results 
for the prototype we implement.

\item
We present AsDroid to statically detect stealthy behaviors in 
Android apps without the knowledge of maliciously destinations
like premium numbers, malicious URLs. We resolve the problem by
inspecting the contradiction between user expectation and program
behaviors. The former one can be abstracted from the user interface
and the latter one is represented by API invocations. We propagate
the intents of API calls to top level functions and then check if 
they mismatch with what the associated user interface indicates. 
The analysis is formalized by datalog rules.
\end{itemize}

\section{Dissertation Organization}

This dissertation is organized as follows: 

Chapter~\ref{chapter:supor} discusses the design, implementation
and evaluation of \uichex. 
Chapter~\ref{chapter:bidtext} details the problem of detecting
sensitive data disclosures for generic data and how we solve 
the problem through bi-directional text correlation analysis. 
Chapter~\ref{chapter:asdroid} talks about the motivation, design and evaluation
of AsDroid to detect stealthy behaviors.
