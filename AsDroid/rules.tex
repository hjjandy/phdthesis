\subsection{Intent Propagation} \label{sec:rules}
In this section, we describe how intents are propagated to top 
level functions such that we can check compatibility with
the corresponding UI text. We also describe how to detect correlation between 
intents. Intent propagation is based on call graph. The calling convention of 
Android apps has its unique features, which need to be properly handled. 
%in order to reveal the proper function invocation correlations. 
Intent correlation 
analysis is mainly based on program dependences. However, correlated 
intents do not simply mean there are (transitive) dependences 
between them.

%%% put it here to make the table appear before its reference (on the same page)
%%% if put it several paragraphs later, the table is on next page and many rules
%%% mentioned are on the page before we can see the rule table.
%\begin{figure}[t]\scriptsize
    \input{AsDroid/datalog-rules}
%\caption{Datalog Rules for Intent Propagation and Correlations} {\label{fig:datalog rule}}
%\end{figure}

The analysis is formally described in the \textit{datalog} language 
\cite{Aho:2006:CPT:1177220}, which is a Prolog-like notation for 
relation computation. It provides a representation for data flow analysis 
in the form of formulated relations. The inference rules on these relations 
are shown in Figure~\ref{fig:datalog_atoms} and Figure~\ref{fig:datalog rule}. 
Relations are in the form 
$p$($X_{1}$, $X_{2}$, ..., $X_{n}$) with $p$ being a predicate. 
$X_{1}$, $X_{2}$, ..., $X_{n}$ are terms of variables or constants.
In our context, variables are essentially program artifacts such as statements,
program variables and function calls. A predicate is a declarative statement
on the variables. For example, \textit{inFunction}($F$,$L$) denotes if a statement 
with label $L$ is in function $F$. 

Rules express logic inferences with the following  form.

{ 
\begin{tabular}{ c }
~~~~~~~~~~~~~~~~~\textbf{$H$ :- $B_{1}$ \& $B_{2}$ \& ... \& $B_{n}$}
\\
\end{tabular}
}

$H$ and $B_{1}$, $B_{2}$,...$B_{n}$ are either relations or negated relations. 
We should read the :- symbol as ``if''. The meaning of a rule is if 
$B_{1}$, $B_{2}$,...$B_{n}$ are true then $H$ is true. 
%A sample inference rule is that 
%$path(L_1, L_3)\ :-\ path(L_1, L_2) \& path(L_2, L_3)$.

Relations can be either inferred or atoms. We often start
with a set of atoms that are basic facts derived from the compiler and 
then infer the other more interesting relations through our analysis.
We use WALA~\cite{Wala:12} % \jjhcomment{(how about Soot?)} 
as the underlying analysis infrastructure. We leverage its 
\textit{single static assignment} (SSA) 
representation, control flow graph, part of call graph, and the 
MAY-points-to analysis to provide the atoms. 

%there are paths
%from statement $L_1$ to $L_2$. If so, we say the pair $L_1$ and $L_2$ is
%in the path relation.
 

%Atom $objectField(X)$ returns true if the tested variable $X$ is an object field (e.g. \texttt{this.d}
%in \autoref{motivating}).
%In the analysis rules, one variable will be seen as a simple local variable in SSA form if not
%specified.
Atom \textit{apiIntent}($L$,$T$) denotes an intent $T$ is associated with an API call at 
$L$, reflecting our API classification. Atom \textit{hasDefFreePath}($L_1$,$L_2$,$X$) 
indicates there is a program path from program point $L_1$ to $L_2$ and 
along the path (not including $L_1$ or $L_2$), variable $X$ may not be
defined. This is to compute the \textit{defUse}($L_1$, $L_2$) relation that 
denotes if a variable is defined at $L_1$ and used at $L_2$. 
To generate the atom relation, we leverage the SSA form and the points-to 
analysis. The analysis is conservative. If we are not sure $X$ must be 
re-defined along the path, we assume the path is definition free. 
The paths we are considering include both intra- and inter-procedural paths.
%are not restricted in the same function boundary.
%For instance, we can there is a path from the entry of \texttt{StartPageActivity.onclick()} to the
%point where \texttt{SmsManager.sendTextMessage()} is invoked in \texttt{ak.a()} in \autoref{motivating}.

%Atom $defPathFree(X,L_1,L_2)$ denotes there is at most one definition of variable $X$ and it must
%be at program point $L_1$. $X$ might not be defined at $L_1$. An implication in this predicate is
%that $hasPath(L_1,L_2)$ is true.

\begin{figure}[t]
\centering
    \begin{tikzpicture}[auto,font=\ttfamily\small]
        \node [rectangle, rounded corners, align=left, draw] (caller) {
            // in method \textit{zjReceiver.onReceive}() \fbox{$F_1$} \\
            Intent intent = new Intent("android.intent.action.RUN"); \\
            \fbox{\hbox to 3mm {$L_1$}} intent.setClass(context, \underline{zjService.class} \fbox{$Y$}); \\
            \fbox{\hbox to 3mm {$L$}}   startService(intent); \fbox{$F_3$($X$)}
        };
        \node [rectangle, rounded corners, align=left, draw, below of=caller, node distance=3.2cm] (callee) {
            // in class \textit{zjService} \fbox{$Y$} \\
            public void onStart(Intent intent, int i) \fbox{$F_2$} \{ 
                $\ldots$ 
            \}
        };
        \draw [thick,->,dashed] (caller) -- (callee);
    \end{tikzpicture}
	\caption{ICC call chain example in app GoldDream.} {\label{fig:icc call example}}
\end{figure}


%\jjhcomment{componentEntry is introduced in next paragraph. so delete the first sentence here.}
%Atom $componentEntry(X,F)$ denotes that $F$ is the entry point of an 
%Android component $X$. 
Android apps are component based.
Generally, there are four types of basic components:
\textit{Activity}, \textit{Service}, \textit{Broadcast Receiver} and 
\textit{Content Provider}. 
%{\bf [XZ: please fill in the following. Use examples if
%needed. Reviewers may not be familiar with Android]} 
Activity component is for a single UI screen. 
Service component is for long-running operations in the background (without 
any UI).  Broadcast receiver responds to system-wide broadcast announcements. 
Content provider is used for application data management~\cite{web:android-guide}.
%During execution, an Android app often fulfills its sub-tasks with 
%components. 
Inter-Component Communication (ICC) 
is used to deliver data between components, which is similar to traditional
function invocations. We have to model such communication as a 
%user interaction 
function may transitively invoke API functions with intent
of interest through ICC. However, the calling convention of ICC is so 
unique that the underlying WALA infrastructure cannot recognize ICC
%function
invocations. Figure~\ref{fig:icc call example} shows an example from a real 
world app \textit{GoldDream}. Inside
the {\tt zjReceiver.onReceive()} function, there is an ICC call to 
the {\tt onStart()} function of the {\tt zjService} component. Observe that
the invocation is performed by creating an Android {\tt Intent} 
object\footnote{{\tt Intent} is a standard class in Android. We call
it Android Intent in order to distinguish with the intents we associate 
with API functions.}, which can be considered as a request that gets 
sent to other components to perform certain actions. The target component
is set by explicitly calling {\tt setClass()} of the Android Intent object.
The request is sent by calling {\tt startService()} with the Android Intent 
object. The Android runtime properly forwards the request to 
the {\tt onStart()} function of the {\tt zjService} component. 

%Unfortunately, this calling convention is invisible to the WALA infrastructure.
To capture such call relation, we introduce the 
\textit{componentEntry}($X$,$F$) atom with $X$  
%subclass of \texttt{android.app.Service} or  \texttt{android.app.Activity}.
a subclass of {\tt Service}, {\tt Activity} or {\tt BroadcastReceiver}.
The entry point $F$ denotes %\jjhcomment{the lifecycle methods including} 
\texttt{onCreate()}, \texttt{onStart()}, and {\tt onReceive()}, 
which are also called \textit{lifecycle} methods by Android developers. 
We introduce atom \textit{iccInvoke}($F_1$,$F_2$,$L$) with $F_2$
denoting special ICC functions, %\texttt{startService()} or {\tt startActivity()}. 
such as {\tt startActivity()}, {\tt startService()} and {\tt sendBroadcast()}.
The second inference  rule of the \textit{invoke}($F_1$, $F_2$,$L$) relation describes
how we model ICC as a kind of function invocation. Let's use the
example in Figure~\ref{fig:icc call example} to illustrate the rule. 
%Function $F_2$ in $iccInvoke()$
%denotes special functions, %\texttt{startService()} or {\tt startActivity()}. 
%such as {\tt startActivity()}, {\tt startService()} and {\tt sendBroadcast()}.
%The component $X$ in $componentEntry()$ must be a 
%subclass of \texttt{android.app.Service} or  \texttt{android.app.Activity}.
%\jjhcomment{sublcass of {\tt Service}, {\tt Activity} or {\tt BroadcastReceiver}.}
%The entry point $F$ denotes \texttt{onCreate()}, \texttt{onStart()}, \jjhcomment{{\tt onReceive()}}
%\texttt{onStartCommand()} 
%and so on. 
It allows us capture the call chain \texttt{zjReceiver.onReceive()} $\rightarrow$ 
\texttt{startService()} $\rightarrow$ {\tt zjService.} {\tt onStart()}.
% which is
%a small part of the corresponding call graph. 
Labels 
{\tiny \fbox{$L$}}, {\tiny \fbox{$L_1$}}, 
{\tiny \fbox{$F_1$}}, {\tiny \fbox{$F_3$}}, and {\tiny \fbox{$Y$}} 
in Figure~\ref{fig:icc call example} correspond to those in the second \textit{invoke}() rule.
%in \autoref{datalog:rule}. 
%The destination of \texttt{startService()} at
%\fbox{$L$} is determined by analyzing the method call at \fbox{$L_1$} which sets \texttt{zjService}
%as the \texttt{class} attribute of the \texttt{intent} object.





% to indicate an ICC call chain. Based on our
%investigation of Android apps, only 
%In this paper, only service components with explicit targets are considered 
%as others do not make API calls that are interesting to us. Hence, $F_2$ 
%must be \texttt{startService()} or {\tt startActivity()}. related method calls and
%$X$ in $componentEntry$ must be a subclass of \texttt{android.app.Service}. The entry point $F$ can be
%\texttt{onCreate}, \texttt{onStart}, \texttt{onStartCommand} and so on.

Atom \textit{directInvoke}($F_1$,$F_2$,$L$) denotes regular function calls including 
virtual calls, leveraging WALA.
% \texttt{a()} calling \texttt{sendTextMessage()} in
%\autoref{motivating}. 
Atom \textit{indirectInvoke}($F_1$,$F_2$) denotes another special kind of 
function invocations in Android apps, namely, implicit calls in thread
execution and event handling. A typical indirect call is a thread-related 
invocation, \eg the actual call destination of \texttt{Thread.start()}
is the \texttt{run()} method of the corresponding class. 
The function call \texttt{f.execute()} $\rightarrow$ \texttt{doInBackground()}
in Figure~\ref{AsDroid:moti:code} (\ie line~\ref{asdroid:moti:line:execute} $\rightarrow$ line~\ref{asdroid:moti:line:doinback}) 
is an example for event handling 
indirect invocation. We detect these implicit calls through pre-defined patterns.
 %thread execution and event handling rules.
% ICC invocation like
%\texttt{startService()} $\rightarrow$ \texttt{onStart()} in \autoref{icc call example} is a
%special kind of indirect call. The difference lies on that in a normal indirect call, the
%destination is an instance pointed to by one of the actual arguments
%directly but ICC destination has to be decided by the inner information of the \texttt{Intent} object of
%the actual arguments, such as \texttt{i} in \texttt{startService(i)}.
%%%%

\comment{
Atoms \textit{isTop}($F$) and \textit{isInteractive}($F$) are used to decide 
the property of function \textit{F}(). Top level
functions are considered as entry points in our analysis and interactive functions are some special
top level functions that only associate with user interactions, such as an implementation of
\texttt{DialogInterface.onClick()} or \texttt{View.OnClickListener.onClick()} as in Figure~\ref{AsDroid:moti:code}.
Other top level functions modeled in the analysis are the ones that can be launched without the
interaction of the application and the user. For example, the \texttt{onCreate()} method of the launcher
Activity and the \texttt{onReceive()} method of a Broadcast Receiver listening to systematic broadcasts are
such kind of top level functions. In the analysis, we first parse \texttt{AndroidManifest.xml} to
get the list of components and then build the top level function list of this kind based on our knowledge.
}

%Rules for $invoke(F_1,F_2,L)$ are used to construct call graph. Compared
%to regular call graphs, we consider ICC calls and indirect calls. 

% in the analysis. $F_1()$ invokes $F_2()$ at
%program point $L$. The basic call relationship is described with a direct invocation. Complex relationships
%include ICC calls and other indirect calls. All these invocations constitute the call graph. As mentioned
%above, ICC calls are special in Android, and we only consider such calls with explicit destination.
%In Android, explicit destination is assigned by designating the \texttt{class} attribute of \texttt{Intent}
%object. \autoref{icc call example} shows the call chain in a real world app GoldDream as
%\texttt{zjReceiver.onReceive()} $\rightarrow$ \texttt{startService()} $\rightarrow$ \texttt{zjService.onStart()} which is
%a small part of the corresponding call graph. Labels \fbox{$L$}, \fbox{$L_1$}, \fbox{$F_1$} and the others are responsible
%to the second $invoke$ rule in \autoref{datalog:rule}. The destination of \texttt{startService()} at
%\fbox{$L$} is determined by analyzing the method call at \fbox{$L_1$} which sets \texttt{zjService}
%as the \texttt{class} attribute of the \texttt{intent} object.

Relation \textit{hasIntent}($F$,$T$,$L$) denotes function $F$ is tagged
with an intent $T$ initiated by the API call at program point $L$.
%and the corresponding API call of intent type $T$ is at $L_2$. Both program points are reachable
%from $F()$ along the call graph. 
For example, in Figure~\ref{AsDroid:moti:code}, we can infer the following:

{%\scriptsize %\footnotesize
\textit{hasIntent} \textbf{(} $F$ = \texttt{StartPageActivity.onClick()}, \\
%\phantom{a$hasIntent$ \textbf{(} } $L_1$ : \texttt{ag.a()}, \\
%\phantom{a\textit{hasIntent} \textbf{(a} } $T$  = \textbf{SendSms}, \\
%\phantom{a\textit{hasIntent} \textbf{(a} } $23$~~ /*\texttt{sm.sendTextMessage(...)}*/~ \textbf{)} = \textsc{True}.
\phantom{\textit{hasIntent}} $T$  = \textbf{SendSms}, \\
\phantom{\textit{hasIntent}} $23$~~ /*\texttt{sm.sendTextMessage(...)}*/~ \textbf{)} = \textsc{True}.
}

Observe that the first \textit{hasIntent}() rule tags the enclosing function of
an API call. The second rule propagates a tag from a
callee to the caller. Note that a function may have multiple intents. These
intents may be of the same type (but initiated at different API call locations).

The remaining relations and rules are for intent correlations.
Relation \textit{correlated}($L_1$,$L_2$) determines if two program points $L_1$ and
$L_2$ are correlated. Correlation can be induced by definition-use,
use-use, and control dependence relations, described by relations 
\textit{defUse}(), \textit{useUse}(), and \textit{controlDep}(), respectively. 
The fourth \textit{correlated}() rule suggests that the relation is transitive.
%returns the result of correlation analysis.
%It tells whether two program points are data or control correlated. There
%are two sub-rules for control dependence analysis and data correlation analysis.

The first rule of \textit{defUse}($L_1$,$L_2$) is standard. In our implementation, we
leverage SSA form to derive definition-use relation for local and
global variables. We leverage points-to relation to reason about 
definition-use relation for object fields. The second rule is to capture 
definition-use relation by parameter passing, including those through
Android specific calling conventions. The basic idea is that we consider
a formal argument $Y$ used inside the callee at $L_2$ is defined at the 
call site $L_1$ (in the caller) if it is not re-defined along the path 
from the callee entry to the use site.

The relation \textit{useUse}($L_1$,$L_2$) denotes that there are uses at $L_1$ and 
$L_2$ coming from the same definition point. For example, $L_1$ and
$L_2$ could be the two uses of the same variable in the two branches of 
a predicate. Considering use-use relation in the \textit{correlated}() relation 
is the key difference from standard program dependence analysis that considers
only definition-use and control dependence relations.


Computation of \textit{controlDep}($L_1$,$L_2$) is standard except that it also models
inter-procedural control dependence. Particularly, all statements in a callee
have control dependence with a predicate in the caller that guards
the call site. 
%extends the 
%basic fact of intra-procedural immediate control dependence relationship to inter-procedural relationship. With this rule, we can say that
%Hence in Figure~\ref{motivating}, \texttt{sm.sendTextMessage()} at line 23 has 
%control dependence with \texttt{if(com.snda.woa.f(this))} at L05 in 
%control dependence with the else branch of the predicate at
%line 2 through the method call
%.because the former one is reachable from the method call
%\texttt{Woa.AG.B()} at line 6.
% which is immediately control dependent on the latter one
%in \texttt{onClick()}.

Finally, the relation \textit{correlatedIntent}($F$,$T_1$,$L_1$,$T_2$,$L_2$) denotes if two
intents $T_1$ and $T_2$ at function $F$ are correlated.

%Rule $dataCorrelated(L_1,L_2)$ is the other correlation rule in the analysis in addition
%to Rule $controlDependent$. Except common data dependence analysis, data correlation
%analysis in our analysis also considers two program points with the same data dependence
%as correlated. All local variables are in static single assignment (SSA) form, which
%makes the data correlation analysis easily. The first rule is correlated to a traditional
%data dependence analysis but the second rule focuses on another type of data correlation:
%two program locations are said data correlated if they use the same SSA variable. Two
%locations in two different branches can be correlated through this rule. The next two rules
%are related to object fields. For the completeness of the rules, we define the rules
%as inter-procedural, but in practice, we make a trade-off that inter-procedural correlation
%analysis for object fields are ignored.


\newbox\shanghaibox
\begin{lrbox}{\shanghaibox}
\begin{minipage}{5in}
\begin{lstlisting}[language=Java, style=customlst, escapechar=|] 
// in class PaySmsActivity 
void a (String v8, String v9, String v10) { 
  SmsManager sm = SmsManager.getDefault();
  ArrayList al = SmsManager.divideMessage(v10);
  Iterator<String> ite = al.iterator(); 
  while (ite.hasNext()) {
    String s = ite.next(); 
    sm.sendTextMessage(v8,v9,s,null,null); |\label{asdroid:sh:line:sendsms}|
  } 
  ContentValues cv = new ContentValues(); |\label{asdroid:sh:line:cv_def}|
  cv.put("address",v8); |\label{asdroid:sh:line:v8_cv}|
  cv.put("body",v10); 
  cv.put("type",2); 
  ContentResolver cr = getContentResolver();
  Uri uri = Uri.parse("content://sms"); 
  cr.insert(uri,cv); |\label{asdroid:sh:line:insert}|
}
\end{lstlisting}
\end{minipage}
\end{lrbox}

\begin{figure}[htp] % from tbp->htp. on bet3, 'tbp' renders it at left-bottom corner whose right side is empty.
\centering
\subfloat [][Code snippet.] {
    \begin{tikzpicture}
        \node [align=left, inner sep=0pt] (code) {\usebox\shanghaibox};
    \end{tikzpicture}
    \label{code snippet}
}
\hfill
	\subfloat [\linespread{1}\selectfont Part of definition-use relations. Solid arrows labeled with variable names indicate def-use relation.] {
    \begin{tikzpicture} [auto,
        loc/.style={rectangle, draw, fill=gray!20},
        line/.style={draw,->,thick}]
        \node at (0,0) [loc, fill=red!20] (L01) {L2};
        \node at (5,-0.7) [loc] (L03) {L4};
        \node at (5,-1.8) [loc] (L04) {L5};
        \node at (5,-2.9) [loc] (L06) {L6};
        \node at (5,-4) [loc, fill=blue!40] (L07) {L8};
        \node at (-5,0) [loc] (L09) {L10};
        \node at (0,-2) [loc] (L10) {L11};
        \node at (-2,-3.0) [loc] (L11) {L12};
        \node at (-5,-4) [loc, fill=blue!40] (L15) {L16};

        \path [line] (L01) -- node {\texttt{v10}} (L03);
        \path [line] (L03) -- node {\texttt{al}} (L04);
        \path [line] (L04) -- node {\texttt{ite}} (L06);
        \path [line] (L06) -- node {\texttt{s}} (L07);
        \path [line] (L01) -- node {\texttt{v8}} (L07);
        \path [line] (L01) -- node {\texttt{v8}} (L10);
        \path [line] (L01) -- node [near end] {\texttt{v10}} (L11);
        \path [line] (L09) -- node {\texttt{cv}} (L10);
        \path [line] (L09) -- node {\texttt{cv}} (L11);
        \path [line] (L09) -- node {\texttt{cv}} (L15);

        \draw [dashed, thick, <->] (L15) to node {\it correlated} (L07);
    \end{tikzpicture}
    \label{data correlation graph}
}
\caption{Intent correlation example in app Shanghai 1930.} {
\label{fig:data correlation example}}
\end{figure}

\smallskip
\noindent
{\bf Example.}
Figure~\ref{fig:data correlation example} %\subref{code snippet}
shows a correlation analysis example in app \textit{Shanghai 1930}.
\texttt{ContentResolver.insert()} at line~\ref{asdroid:sh:line:insert} stores the sent text message into
the mail box and it hence has intent type \textbf{SmsNotify}. It is determined
to be correlated to the SMS sending operation with {\bf SendSms} intent 
at line~\ref{asdroid:sh:line:sendsms}. According to the 
definition-use graph in Figure~\ref{fig:data correlation example}\subref{data correlation graph},
%L15 is correlated with L10 and L11 (all use $cv$ defined at L9) by the
%$useUse()$ rules.  L10 and L11 are further correlated with L7 because
%of variables $v8$ and $v10$, again by the $useUse()$ rules. 
line~\ref{asdroid:sh:line:insert} is correlated with line~\ref{asdroid:sh:line:v8_cv}
(both use \textit{cv} defined at line~\ref{asdroid:sh:line:cv_def}) by the
\textit{useUse}() rules. Line~\ref{asdroid:sh:line:v8_cv} is further correlated with line~\ref{asdroid:sh:line:sendsms} because
of variables \texttt{v8}, again by the \textit{useUse}() rules. 
Hence, we have 
\textit{correlatedIntent}(\texttt{PaySmsActivity.a()}, \textbf{SendSms},
\ref{asdroid:sh:line:sendsms}, \textbf{SmsNotify}, \ref{asdroid:sh:line:insert})=\textsc{True}. Intuitively, the two intents are correlated
because the same content is being sent over a short message and written to the
mail box. Thus, the message send is not stealthy.

% L1 through variables \texttt{v8} and \texttt{v10} respectively. L7 is directly data
%dependent on L1 via \texttt{v8} and indirectly data dependent on L1 via \texttt{v10}.
%By applying the rules in \autoref{datalog:rule}, we can get that
%$correlated (L7, L15) = \textsc{True}$.

%%% if put this figure before 'Example', it occupies the right bottom corner of that
%%% page and 'Example' part is at the top of next page. No differences here.


%\autoref{data correlation example} also shows an example for Rule
%$smsNotified(F,L)$ which denoted if there is a program point $L$
%reachable from $F()$ has intent type \textbf{SmsNotify}. This rule
%uses $dataCorrelated$ instead of $correlated$ because we want to make
%sure that the SMS sent out is the same as the one notified to the user.

\comment{
\jjhcomment{
	{\bf Example 2.}
	Figure~\ref{fig:http correlation example} presents an example for HTTP outcome
	flowing to a GUI component in \textit{Special Forces}. 
	The HTTP access is made at line 6 and line 7. The outcome is acquired
	at line 8 and then flows to line 9 where it is converted to a bitmap. The bitmap
	is returned to line 2 and then line 13. Then it is assigned to a static field {\tt e.q}.
	{\tt e.q} is used at line 24 where the bitmap is set into an {\tt ImageView}
	which is a part of a dialog. Numbers {\bf 1} to {\bf 6} show the data flow.
	Propagating intents {\bf HttpAccess} at line 8 to line 13 
	and {\bf UiOperation} at line 24 to line 15 in method {\tt e\$3.run()}, 
	we have \textit{correlatedIntent}(\texttt{e\$3.run()}, \textbf{HttpAccess}, 13,
	 \textbf{UiOperation}, 15)=\textit{true}.
}

\begin{figure}[htp]
\centering
%\subfloat [][Code Snippet] {
    \begin{tikzpicture}[font=\scriptsize\bfseries\ttfamily]
        \node [rectangle, align = left] (code) {
			// in class \textit{com.jasonwu.pushads.e} \\
			01: static Bitmap a (String str)\{ \\
			02: \hspace{2pt}	return b(str); // {\bf (3)} \\
		    03: \} \\
			04: static Bitmap b (String str)\{ \\
			05: \hspace{2pt}	URL url = new URL(str); \\
			06: \hspace{2pt}	HttpURLConnection conn = url.openConnection(); \\
			07: \hspace{2pt}	conn.connect(); \\
			08: \hspace{2pt}	InputStream is = conn.getInputStream(); // {\bf (1)} \\
			09: \hspace{2pt}	return BitmapFactory.decodeStream(is); // {\bf (2)} \\
			10: \} \\
			// in class \textit{com.jasonwu.pushads.e\$3} \\
			11: void run ()\{ \\
			12: \hspace{2pt}	String url = e.f(); \\
			13: \hspace{2pt}	Bitmap bm = e.a(url); // {\bf (4)} \\
			14: \hspace{2pt}	e.q = bm; // {\bf (5)} \\
			15: \hspace{3pt}	e.d().sendEmptyMessage(2); //$\rightarrow$ \textit{e\$1.handleMessage()}\\
			16: \} \\
			// in class \textit{com.jasonwu.pushads.e\$1} \\
			17: void handleMessage (Message msg)\{ \\
			18: \hspace{2pt}	f.a(); \\
			19: \} \\
			// in class \textit{com.jasonwu.pushads.f} \\
			20: static void a ()\{ \\
			21: \hspace{2pt}	AlertDialog.Builder b = new AlertDialog.Builder(...); \\
			22: \hspace{2pt}	View v = e.j.getLayoutInflater().inflate(...); \\
			23: \hspace{2pt}	ImageView iv = v.findViewById(...); \\
			24: \hspace{2pt}	iv.setImageBitmap(e.q); // {\bf (6)} \\
			25: \hspace{2pt}	b.setView(v); \\
			26: \hspace{2pt}	b.create().show(); \\
			27: \} \\
        };
    \end{tikzpicture}
%    \label{code snippet}
%}
	\caption{HTTP outcome flows to a GUI component in \textit{Special Forces}.} 
	{\label{fig:http correlation example}}
\end{figure}
}

\comment{
Rule \textit{violated}($F$,$T_1$,$T_2$) is the top level rule in the analysis. It returns
if intent type $T_1$ held by \textit{F}() is violated. The 4 rules can be separated
into two sub-types. The first two are correlated with SMS sending operations after
user interaction and the others are related to sending SMS or phone call
without user interaction.

When a SMS is sent out after user interaction with the application interface,
the analysis reports a violation only if the corresponding SMS is not notified
and the same top level function holds uncorrelated and incompatible intent types,
e.g. \textbf{UiOperation}. When a SMS is sent out without user interaction, it is
reported with a violation if it is not notified. For a phone call without user
interaction, it is directly reported. For the example in Figure~\ref{motivating},
we can say that \textit{violated}(\texttt{onClick()}, SendSms, HttpAccess)
= \textsc{True} after applying the \textit{Datalog} rules to it. In Figure~\ref{fig:data correlation example},
assume \texttt{a()} is called by a top level function \texttt{t()} (\texttt{t} can
be either an interactive function, e.g. \texttt{onClick}, or an app entry function, e.g. \texttt{Activity.onCreate}),
then we can get $violated(\texttt{t()},SendSms, T_2) = \textsc{False}$ because
$smsNotified(\texttt{t()}, L15)$ returns \textsc{True} for every rule on intent
type \textbf{SendSms}.

}
