\subsection{UI Compatibility Check} {\label{sec:compatibility}}
After intents are propagated to top level functions, the next step is to
check their compatibility with the text of the corresponding user interface
artifacts.

\smallskip
\noindent
{\bf Acquiring User Interface Text}. Given a top level function, we need to
first extract the corresponding text. User interface components in an 
Android app are organized in a view tree.  A view is an object that renders 
the screen that the user can interact with. Views can be organized
as a tree to reflect the layout of interface. There are two ways to construct
the layout: (1) statically through an XML resource file; (2) dynamically
by constructing the
view tree at runtime. 
%The static approach is more popular. 

With the static layout construction, upon the creation of an activity, 
the corresponding user interface is 
instantiated by associating the activity with the corresponding XML 
file by calling {\tt setContentView([XML layout id])}. The Android
core renders the interface accordingly. A UI object has a unique ID. 
The ID is often specified in the XML file. Inside
the app code, the handle to a UI object is acquired by calling 
{\tt findViewById([object id])}. For example, the following text defines
a button in the XML file. Note that the button text is also specified.

\begin{lstlisting}[language=XML,style=customlst]
  <Button android:id="@+id/my_button"...
       android:text="@string/my_button_text"/>
\end{lstlisting}

Its handle can be acquired as follows. Note that the lookup id matches with that in the XML file.

\begin{lstlisting}[language=Java,style=customlst]
  Button btn = (Button)findViewById(R.id.my_button);
\end{lstlisting}

The event handler for an UI object is registered as a listener. For example, 
one can set the listener class for the previous button by making the following
call.
\begin{lstlisting}[language=Java,style=customlst]
  btn.setOnClickListener(new MyListener(...));
\end{lstlisting}

In this case, the {\tt onClick()} method of the {\tt MyListener} class becomes
the top level user interaction function associated with the button. Next we
describe how we extract text for different kinds of functions.

\smallskip
For a top level \textit{interactive} function \textit{F} (e.g. {\tt onClick()}), 
AsDroid identifies the 
corresponding UI text as follows. It first identifies the registration point 
of the listener class of \textit{F}. From the point, AsDroid acquires 
the UI object handle, whose ID can be acquired by finding the corresponding 
{\tt findViewById()} function. The ID is then used to scan the layout XML file
to extract the corresponding text. AsDroid also extracts the text in
the parent layout.
% because the parent layout often displays it is also visible to the user. 
For example, the parent layout of a button may be a
dialog. Important information may be displayed in
the dialog and the button may have only some simple text such as ``{\tt OK}''.
We currently cannot handle cases in 
which the text is dynamically generated. We found such cases are  
relatively rare.

\comment{
To handle cases that the corresponding UI object is dynamically constructed
(not configured by an XML file), AsDroid also looks for all the text 
setting methods (e.g. {\tt setText()} and {\tt setMessage()}) associated with
the UI object and extract the text. We currently cannot handle cases in 
which the text is dynamically generated. We found such cases are  
rare in practice.
}

\smallskip
Some \textit{non-interactive} top level functions also have associated UIs, for
instance, the lifecycle methods {\tt onCreate()} and {\tt onStart()} of
activity components. These methods are invoked when the screen of an activity
is first displayed. While no user interactions are allowed when executing these
methods, the displayed screen may have enough information to indicate the 
expected behavior of these methods, such as loading data from a 
remote server. Hence, for an activity lifecycle method,
AsDroid extracts the text in the XML layout file associated with the activity.
  
%\jjhcomment{If a top level function is an activity's lifecycle method, all text
%that can be found in the UI would be extracted. If a top level function is
%irrelavant to any UI, (e.g. {\tt onReceive()}), no associated 
%UI text is extracted.}
%If a top level function is not associated with any UI object, such as the 
%entry function of an application, no text is extracted.

%\smallskip
%Furthermore, some screen prompts might be displayed during executions 
%to inform the user about some background behavior. For example, 
%{\tt Toast.makeText()} is commonly used for this purpose, which pops up
%a text box \xyzcomment{Make sure what I say is correct}. Note that such 
%methods are not  Our technique tries to extract all such 
%prompts along the paths that flow through the sensitive API calls.} 
 
\smallskip
\noindent
{\bf Text Analysis.}
Once we have the text, we build a dictionary that associates a type of 
intent to a set of keywords through training. We use half of the apps 
from the benign sources\footnote{We collect apps from both benign and 
malicious sources as shown in Section~\ref{sec:evaluation}.} as the 
training subjects, which account for about 28\% of all the
apps we study.  During evaluation, we use the dictionary generated from 
the 28\% apps to scan over the entire set of apps. 
Here, we assume the training apps are mostly benign. If an intent appears 
together with some text in a benign case, then the intent and the text 
are compatible. We use keywords to represent text, and build compatible 
keyword cover set for each intent.
%, which can help us remove false alarms.
In particular, For each intent type \textit{T} of interest, we identify all 
the top level functions \textit{F} that have \textit{T} annotated and collect their 
corresponding texts. We then use Stanford Parser~\cite{Levy:2003:VCL:Parser}
to parse the text to keywords. We populate a universal set \textit{S} to include 
all individual keywords and keyword pairs that appear in these functions. 
%%
\comment{ {\bf [JH: If a combination of two or more keywords is more semantically related to the 
intent than any single keyword, these keywords are paired. Keywords with similar 
meaning in the text will be represented by one keyword. For example, 
``Send + Sms'' is more cohesive than a single ``Send''. Multiple occurrences of 
single ``Send'' and ``Text'' are represented by ``Send'' in Figure~\ref{fig:keyword}. ]} }
%%
We then use Algorithm~\ref{algo:training}
to identify the smallest set of keywords (or pairs) that have the highest 
frequency and cover all the top level functions tagged with \textit{T}.

\begin{algorithm}[t]
\scriptsize
train($S$, $F$){
\begin{algorithmic}[1]
\STATE $\mathit{KWD}$=$\phi$ /*the keyword cover set*/
%\STATE $t=|F|$
\WHILE{$F\neq\phi$}
   \STATE sort $S$ by keyword (or keyword pair) frequency 
   \STATE $k$=the top ranked keyword (or pair) in $S$
   \STATE $X$= the functions in which $k$ occurs
   \STATE $\mathit{KWD}$=$\mathit{KWD}\cup k$
   \STATE $F$= $F$-$X$
   \STATE $S$=$S$-\{all the keywords (pairs) in $X$\}
\ENDWHILE
\end{algorithmic}
}
\caption{Generating Keyword Cover Set.}
\label{algo:training}
\end{algorithm} 

The algorithm is similar to the greedy set cover algorithm~\cite{Cormen:2009:IAT:1614191}. 
It picks the most frequently occurring keyword $k$ at a time 
and adds it to the keyword set. Then it removes all the keywords 
that appear in the top level functions in which $k$ occurs, as they can
be covered by $k$. It repeats until the set of functions are covered. 

We consider keyword pairs are semantically more predictive. Hence, we first 
apply the algorithm to keyword pairs and keep the pairs that
can uniquely cover at least 10\% of functions.
% \xyzcomment{is the 1\% correct?}
%\jjhcomment{when I select keyword pairs, I simply select those with highest frequency. I didn't care how many functions they can cover but from the result, they do cover a lot.}.
Then we apply the algorithm to singleton keywords on the remaining functions.

\begin{figure}[t]
\centering
    \includegraphics[angle=-90,clip,width=0.95\textwidth]{figs/KeywordsFrequency}
	\caption{\linespread{1}\selectfont The keyword cover set for the SendSms intent. The y-axis
denotes the percentage of top level functions that can be \textit{uniquely} 
covered by a keyword (pair). }\label{fig:keyword}
\end{figure}
%Probability of SMS send for keyword patterns. `+' means combination. `A' + `B' means A and B
%    appear simultaneously.

\comment{
Figure~\ref{fig:keyword} shows the generated keyword cover set for 
{\bf SendSms} intent. Observe most keywords are semantically related to 
the intent except ``OK'' and ``Registration+Login'', which occur rarely but do uniquely cover some functions. 
Further inspection shows that it is due to the malwares in the training pool.
Hence, we also use human semantic analysis to prune the keyword set, e.g.
filtering out ``OK'' and ``Registration+Login''.
The cover set of {\bf PhoneCall} is much simpler, containing only one 
keyword ``Call''. 
}

Figure~\ref{fig:keyword} shows the generated keyword cover set for the 
{\bf Send}-{\bf Sms} intent. Observe some keywords are semantically related to 
the intent but some are not, e.g. ``OK'' and ``Register'', which occur rarely but do uniquely cover some functions. 
Further inspection shows that it is due to the malwares in the training pool.
Hence, we also use human semantic analysis to prune the keyword set, e.g.
filtering out ``OK'' and ``Register''. 
%{\bf [JH: Some keywords are related to the intent
%but they have a relatively low frequency, so at last the cover set contains
%only the three keywords (or keywords pairs) with highest frequency.]}
The keyword set of {\bf HttpAccess} is similarly constructed, containing
keywords  ``Download'', ``Login'', ``Load'', ``Register'', and so on.
% update, network,
%internet, webpage
The cover set of {\bf PhoneCall} is much simpler, containing only one 
keyword ``Call''. %\xyzcomment{Are we going to say sth. for the keywords of
%http accesses?}\jjhcomment{generally, when I try to identify if the text indicates
%HTTP access, I use keywords including: download, login, loading, register, update, network,
%internet, webpage ... I didn't do the step mentioned here but these keywords are frequently used.}

Once we get the keyword cover set, we further populate it with its synonyms,
using 
%WordNet and 
Chinese WordNet~\cite{web:cn:wordnet} to have the final dictionary.
%contains the set of words that hint 
%the in
%{\bf [XZ: we should fill in this part after discussing with prof. Lin Tan}.

%\bf [XZ: the compatibility table goes here]}

\smallskip
\noindent
{\bf Compatibility Check.}
%Given top level functions annotated with intents, intent correlation,
%and the corresponding GUI text, t
The compatibility check is performed as follows. 
\begin{itemize}
\item Given a top level function \textit{F} with UI text \textit{S} and
an intent \textit{T}, if \textit{S} is incompatible with \textit{T} and all the intents
%xxxor any intent $T'$ 
correlated with \textit{T}, it is considered a mismatch. Note that we consider
empty text is incompatible with any intent. 

\item If \textit{T} is a {\bf SendSms} intent and has a 
correlated {\bf SmsNotify} intent. It is not a mismatch 
regardless of the UI text. 

\item If \textit{T} is {\bf HttpAccess}, the technique checks if the corresponding
UI text is compatible. If not, it further checks if \textit{T} is correlated to 
any {\bf UiOperation} intent. If not, the intent is consider stealthy.
Intuitively, it suggests that even an HTTP access is not explicit from the
GUI text, if the data acquired through the HTTP connection are used in 
some UI component (e.g. fetching and then displaying advertisements 
from a remote server), the HTTP access is not considered stealthy.

%Since many benign apps have silent HTTP accesses, it
%is very difficult to distinguish such benign accesses from malicious accesses,
%even with the help of text analysis, we currently assume {\bf HttpAccess} 
%intent is compatible with any text. We leave it to our future work to develop
%a better solution for stealthy malicious HTTP accesses.

%\item Given a top level function $F$ with empty UI text and an intent $T$,
%if $T$ is a {\bf SendSms}, {\bf Install} or {\bf PhoneCall} intent, it 
%is considered a violation.
\end{itemize}

\comment{
For the example in Figure~\ref{motivating}, a mismatch is reported. 
The {\tt onClick()} function at line~\ref{asdroid:moti:line:onclick} is tagged with {\bf SendSms} and {\bf HttpAccess}
intents. The two intents are not correlated. While the UI text ``{\tt One-Click
Register \& Login}'' is compatible with {\bf HttpAccess}, it is not with 
{\bf SendSms}. 
%we can say that $violated(\texttt{onClick()}, SendSms, HttpAccess)
%= \textsc{True}$ after applying the \textit{Datalog} rules to it. 
In Figure~\ref{fig:data correlation example}, assume \texttt{a()} at line~\ref{asdroid:moti:line:onclick} 
is called by a top level function \texttt{t()}. No mismatch is reported 
as the  {\bf SendSms} and {\bf SmsNotify} intents are correlated. 
}

%For the second rule of the compatibility check, we do not consider
%{\bf HttpAccess} or {\bf UiOperation} intents because it is very common that
%HTTP connections and  

% (\texttt{t} can
%be either an interactive function, e.g. \texttt{onClick}, or an app entry function, e.g. \texttt{Activity.onCreate}),
%then we can get $violated(\texttt{t()},SendSms, T_2) = \textsc{False}$ because
%$smsNotified(\texttt{t()}, L15)$ returns \textsc{True} for every rule on intent
%type \textbf{SendSms}.



