%
%  revised  front.tex  2017-01-08  Mark Senn  http://engineering.purdue.edu/~mark
%  created  front.tex  2003-06-02  Mark Senn  http://engineering.purdue.edu/~mark
%
%  This is ``front matter'' for the thesis.
%
%  Regarding ``References'' below:
%      KEY    MEANING
%      PU     ``A Manual for the Preparation of Graduate Theses'',
%             The Graduate School, Purdue University, 1996.
%      PU8    ``A Manual for the Preparation of Graduate Theses'',
%             Eighth Revise Edition, Purdue University.
%      TCMOS  The Chicago Manual of Style, Edition 14.
%      WNNCD  Webster's Ninth New Collegiate Dictionary.
%
%  Lines marked with "%%" may need to be changed.
%

  % Statement of Thesis/Dissertation Approval Page
  % This page is REQUIRED.  The page should be numbered page ``ii''
  % and should NOT be listed in your TABLE OF CONTENTS.
  % References: PU8 ordinal pages 5 and 29.
  % The web page https://engineering.purdue.edu/AAE retrieved on
  % January 8, 2017 had "School of Aeronautics and Astronautics"---that
  % is used instead of "Department af Aeronautics and Astronautics"
  % below.
\begin{statement}
  \entry{Dr.~Xiangyu Zhang, Chair}{Department of Computer Science, Purdue University}
  \entry{Dr.~Ninghui Li}{Department of Computer Science, Purdue University}
  \entry{Dr.~Vernon J. Rego}{Department of Computer Science, Purdue University}
  \entry{Dr.~Lin Tan}{Department of Electrical and Computer Engineering, University of Waterloo}
  \approvedby{Dr.~Voicu S. Popescu by Dr.~William J. Gorman}{Head of the School Graduate Program}
\end{statement}

  % Dedication page is optional.
  % A name and often a message in tribute to a person or cause.
  % References: PU 15, WNNCD 332.
\begin{dedication}
	This work is dedicated to my wife Jie Zhang. 

\begin{figure}[h]
	\centering
	\vspace{1cm}
	\includegraphics[angle=-90,clip]{figs/dedication.eps}
\end{figure}
\end{dedication}

  % Acknowledgements page is optional but most theses include
  % a brief statement of apreciation or recognition of special
  % assistance.
  % Reference: PU 16.
\begin{acknowledgments}
	\input{ack}
\end{acknowledgments}

  % The preface is optional.
  % References: PU 16, TCMOS 1.49, WNNCD 927.
%\begin{preface}
%  This is the preface.
%\end{preface}

  % The Table of Contents is required.
  % The Table of Contents will be automatically created for you
  % using information you supply in
  %     \chapter
  %     \section
  %     \subsection
  %     \subsubsection
  % commands.
  % Reference: PU 16.
\tableofcontents

  % If your thesis has tables, a list of tables is required.
  % The List of Tables will be automatically created for you using
  % information you supply in
  %     \begin{table} ... \end{table}
  % environments.
  % Reference: PU 16.
\listoftables

  % If your thesis has figures, a list of figures is required.
  % The List of Figures will be automatically created for you using
  % information you supply in
  %     \begin{figure} ... \end{figure}
  % environments.
  % Reference: PU 16.
\listoffigures

  % List of Symbols is optional.
  % Reference: PU 17.
%\begin{symbols}
%  $m$& mass\cr
%  $v$& velocity\cr
%\end{symbols}

  % List of Abbreviations is optional.
  % Reference: PU 17.
\begin{abbreviations}
  API & Application Program Interface	\cr
  JSON & JavaScript Object Notation	\cr
  UI & User Interface	\cr
  GUI & Graphical User Interface \cr
  URL & Uniform Resource Locator	\cr
  WALA & T. J. Watson Libraries for Analysis \cr
  APK & Android Package Kit \cr
  DEX & Dalvik Executable Format \cr
  ICC & Inter-Component Communication \cr
  SDK & Software Development Kit \cr
  ADT & Android Development Tools \cr
  SMS & Short Message Service \cr
  IR & Intermediate Representation \cr
  SSA & Static Single Assignment \cr
  IMEI & International Mobile Equipment Identity \cr
  NLP & Natural Language Processing \cr
  APP & (Android) Application \cr
  WYSIWYG & What You See Is What You Get \cr
  IDE & Integrated Development Environment \cr
  LHS & Left Hand Side \cr
  RHS & Right Hand Side \cr
  FP & False Positive \cr
  TP & True Positive \cr
  FN & False Negative \cr
\end{abbreviations}

  % Nomenclature is optional.
  % Reference: PU 17.
%\begin{nomenclature}
%  Alanine& 2-Aminopropanoic acid\cr
%  Valine& 2-Amino-3-methylbutanoic acid\cr
%\end{nomenclature}

  % Glossary is optional
  % Reference: PU 17.
%\begin{glossary}
%  chick& female, usually young\cr
%  dude& male, usually young\cr
%\end{glossary}

  % Abstract is required.
  % Note that the information for the first paragraph of the output
  % doesn't need to be input here...it is put in automatically from
  % information you supplied earlier using \title, \author, \degree,
  % and \majorprof.
  % Reference: PU 17.
\begin{abstract}
While smartphones and mobile apps have been an integral part of 
our life, personal security issues on smartphones become a serious concern. 
Privacy leakage, namely sensitive data disclosures, 
happens frequently in mobile apps to disclose the user's 
sensitive information to untrusted, even malicious, third-party
service providers, leading to serious problems. 
Besides, stealthy behaviors that are performed without the user's 
acknowledgment may cause unexpected phone charges or leakage of 
sensitive information.

To address these problems, many approaches have been proposed.
However, previous mobile privacy related research efforts have largely 
focused on predefined known sources managed by smartphones. 
More specifically, they focus on the API functions that directly return 
sensitive values. Some other information sources, such as the 
user inputs through user interface and data obtained from network 
or files, have been mostly neglected, even though such sources 
may contain a lot of sensitive information. 
In addition, the research efforts on detecting stealthy behaviors
also depend on identifying suspicious
behaviors with known actions, e.g., known premium phone numbers
or URLs of malicious websites.

In this dissertation, we present two automated techniques for the 
purpose of comprehensively sensitive data disclosure detection.
Moreover, we propose a novel technique to detect stealthy behaviors in 
Android apps.

Firstly, we examine the possibility of scalably detecting 
sensitive user inputs from mobile apps. We design and implement
\uichex, a novel static analysis tool that automatically examines
the user interface to identify sensitive user inputs containing 
critical user
data, such as user credentials, finance and medical data. \uichex
mimics from the user's perspective to associate input fields in
user interfaces with most correlated text labels and utilizes 
text analysis to determine the sensitiveness of the user inputs.
With the knowledge of sensitive user inputs, we are then able to
detect their disclosures with the help of taint analysis.

Secondly, we develop \bidtext to address the issues of detecting 
sensitive data disclosures where the data is generated by generic
API functions whose return values cannot be easily recognized as 
sensitive or insensitive. \bidtext leverages the context of the 
data, associates the correlated text labels to corresponding 
variables and then applies text analysis to determine the 
sensitiveness of the data held by the variables. The intuition here 
is that the context of programs contains useful information to 
indicate what the variables may hold. \bidtext also 
features a novel bi-directional propagation technique through 
forward and backward data-flow to enhance static sensitive data disclosure 
detection.

Thirdly, we develop AsDroid to detect stealthy behaviors in Android 
apps by checking the contradiction between user expectation, which
is represented by user interface, and program behavior that can
be abstracted by API invocations. We model API invocations with 
different types of intents and backwardly propagate the intents 
to top level functions, e.g., a user interaction function. We then
analyze the text extracted from the user interface component associated
with the top level function. Semantic mismatch of the two indicates
stealthy behavior.

To sum up, in this dissertation, we present SUPOR to detect sensitive
user inputs, and \bidtext to determine the sensitiveness of the 
data generated by generic API functions. We also propose 
bi-directional propagation to enhance sensitive data 
disclosure detection. In addition, we inspect the contradiction 
between program behaviors and user expectations to detect 
stealthy behaviors in Android apps.
\end{abstract}

