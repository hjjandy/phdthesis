# $Id: Makefile,v 1.1 2010/12/02 sgamage Exp $

LATEX   = latex
BIBTEX  = bibtex
METAPOST= mpost
PIC     = pic
JGRAPH  = jgraph

PICOPTS = -t

MAINFILE= thesis
TEXSRCS	= thesis.tex 
PICSRCS	= 
FIGS    = 
JSRCS   = 
PPTS    = 

all: $(MAINFILE).ps $(MAINFILE).pdf

graphs: allgraphs.tex
	latex allgraphs.tex
	dvips -Pdownload35 -t letter -o allgraphs.ps allgraphs.dvi

%.dvi: %.tex
	@if [ ! -r $(subst tex,aux,$<) ]; then $(LATEX) $<; fi
	$(LATEX) $<

figs/%.1: figs/%.mp
	cd fig ; TEX=$(LATEX) $(METAPOST) $(subst fig/,,$<)        

figs/%.eps: figs/%.j
	$(JGRAPH) $< > figs/$*.eps

%.tex: figs/%.pic
	$(PIC) $(PICOPTS) $< > $*.tex

$(MAINFILE).dvi: $(FIGS) $(PPTS) $(PICSRCS) $(JSRCS) $(MAINFILE).bbl \
	$(TEXSRCS) always

%.ps: %.dvi
	dvips -t letter -o $*.ps $< 

%.pdf: %.ps
	ps2pdf $*.ps 

%.tex: %.pic
	$(PIC) $(PICOPTS) $< > $*.tex

$(MAINFILE).bbl: all.bib *.tex BidText/*.tex SUPOR/*.tex figs/*.eps
	#$(LATEX) $(MAINFILE)
	#$(BIBTEX) -min-crossrefs=100 $(MAINFILE)
	#$(LATEX) $(MAINFILE)
	$(LATEX) $(MAINFILE)
	$(BIBTEX) $(MAINFILE)
	$(LATEX) $(MAINFILE)
	$(BIBTEX) $(MAINFILE)
	$(LATEX) $(MAINFILE)

always:
	@:

slides:	$(SLIDES).ps 

$(SLIDES).dvi: $(FIGS) $(TRACES) $(PICSRCS) always

clean:
#	rm -f *.dvi *.aux *.log *.blg *.bbl $(MAINFILE).ps $(PICSRCS) $(FIGS) 
	rm -f *.dvi *.aux *.log *.blg *.out $(MAINFILE).ps $(PICSRCS) $(FIGS) *.bbl\
	 $(MAINFILE).pdf *~ *.toc *.lot *.lof SUPOR/*~ BidText/*~ AsDroid/*~\
	 SUPOR/\.*.un~ BidText/\.*.un~ AsDroid/\.*.un~ \.*.un~
#	fig/*.log fig/*.aux fig/*.dvi fig/*.mpx *.tmp $(SLIDES).ps $(JSRCS)


