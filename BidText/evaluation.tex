\section{Evaluation} \label{sec:eval}

All experiments are performed on an Intel Core i7 3.4GHz machine 
with Ubuntu 12.04. The task of analyzing each app is given 
the maximum memory of 10GB and the maximum analysis time of 20 minutes. 
The subjects are a collection of 10,000 Android apps downloaded from 
Google Play in March 2015. The sink points used in the evaluation 
contain all the logging operations in Android and the Apache HTTP 
access APIs that are commonly used in Android apps. This is also the
standard setup for many existing static taint analysis~\cite{Huang:SUPOR:2015, AsDroid:14}.
The other types of sink points can be easily added to \bidtext. 

\subsection{Pilot Study}
As discussed earlier, \bidtext heavily relies on accurate propagation models 
for API method calls. However, Android framework contains 
thousands of API functions, making it almost infeasible to manually build
the models for all API functions. Our approach is to randomly select 2,000
apps and run \bidtext on these apps. Then we inspect the results to 
discover popular API functions and create models only for those functions.
These models are later used in the larger scale study.

During the pilot study, we also observe a kind of false positive that 
appears frequently. It is caused by a Facebook library used by many apps. 
The library logs an error message when it fails to obtain the device Id.
The code snippet is abstracted as follows.
%\newbox\phicodeexample
%\begin{lrbox}{\phicodeexample}
\begin{lstlisting}[language=Java,style=customlst,escapechar=|]
try { 
  /* acquire device id */ 
} catch (Exception e) { 
  Utility.logd("android_id", e); 
}
\end{lstlisting}
%\end{lrbox}
The message {\tt e} is typed with ``{\it android\_id}'', 
which is a sensitive keyword. But the meaning of this message is 
indeed that the action of acquiring the device Id fails. 
Solving this issue requires in-depth semantic analysis of {\tt e} which is
not supported by \bidtext. 
% but the logged 
%data do not contain any device ID information. Because all such cases 
Since the pattern is fixed, we post-process all the reports to filter out
this pattern for both the pilot study and the later large scale study.
%have the same pattern, it is easy for us to remove the false alarms via 
%a post-process step.

\subsection{Unification vs. Bi-directional Propagation}
In classic type inference, given an assignment statement such as {\tt z=x+y} 
and {\tt z=$\phi$(x,y)}, the updated type sets of {\tt x}, {\tt y}, and {\tt z} 
are the union of all three original type sets. In Section~\ref{sec:subsub:prop_assign} (Rules 
Binary-Assignment and Phi-Assignment in Figure~\ref{fig:propagation}),
we mentioned that such a unification based approach may produce a lot of
false positives and hence \bidtext makes use of a bi-directional 
propagation strategy that avoids propagating type sets between right-hand-side
operands (\ie {\tt x} and {\tt y} in the example).
In this experiment, we want to compare these two propagation strategies.
% That is, all three
%variables have the same type. 
%We want to measure the effect of the bi-directional propagation policy
%we adopt for $\phi$ statement in Figure~\ref{fig:propagation}, which we 
%call {\it selectively} bi-directional propagation. The other choice is 
%{\it fully} bi-directional propagation, which generates
%\ctxevalN{var$_1$} = \ctxeval{var$_1$} $\cup$ \ctxeval{var} for statement 
%{\tt var = $\phi$(var$_1$, var$_2$)}.

Due to the lack of ground truth, such a study requires manually inspecting
the reported disclosure defects and determining if they are false positives.
Among the 2000 apps tested in the pilot study, we selected the first 60 apps
whose {\em data disclosure path} (\ie the data flow subgraph that includes 
the path from the source to the sink and the path that the sensitive text
is propagated from its origin to the sink) involves $\phi$ statements
and/or binary operations with the unification based propagation policy.
% in the propagations paths. 
We re-run \bidtext on the 60 apps with the bi-directional propagation policy
and compare the two sets of results.
% ({\it fully} or 
%{\it selectively}) and have the following discoveries.

Among these 60 apps, 42 of them are reported by both the unification policy 
and the bi-directional policy; 25 of them contains flows only reported by the 
unification policy. Note that the two do not add up to 60 because some apps have 
multiple reported disclosures, some being reported by both policies and the others
 being only reported by the unification policy. 
We manually studied the 25 cases reported by the unification policy and found
that they are all false positives. 
We have shown one sample false positive in Section~\ref{sec:subsub:prop_assign}.% In the following,
%we show another example observed in the real apps XXX%.

%have disclosures caused by forward data flow
% contains forward-only paths using both policies. That means 
%different choices of the propagation policy do not affect the results 
%of those paths. 25 apps contains bi-directional propagation paths using 
%policy {\it fully} while all these paths disappear using policy 
%{\it selectively}. 4 apps have the same paths involving backward propagation
%over $\phi$ statements for both policies. We inspected the differences 
%between applying two policies and conclude that {\it selectively} 
%bi-directional propagation produces more accurate results.

\comment{
However, in app {\it com.a97704234054e44509233154a.a62629891a}, 
we find a false negative caused by {\it selectively} bi-directional 
propagation. The simplified code snippet is as follows:

\begin{lstlisting}[style=customlst, escapechar=|]
id = sharedPref.getString("fpopenudid", null); |\label{line:fn:get}|
if (id == null) {
  id = getOpenUDID(); // sensitive |\label{line:fn:src_tag}|
  sharedPref.edit().putString("fpopenudid", id); |\label{line:fn:put}|
}
\end{lstlisting}

The code first tries to obtain a value from a persistent data storage
(an instance of {\tt SharedPreferences}). If the value does not exist yet,
the code queries from the system and then puts the data into the persistent 
storage. Since both {\tt get} and {\tt put} operations use the same key 
in the storage, we can conclude that \varL{id}{line:fn:get} 
holds the same value as the one at line \ref{line:fn:src_tag} after the 
first run of this code snippet. Therefore, \varL{id}{line:fn:get} should be 
treated sensitive. \bidtext tags \varL{id}{line:fn:src_tag} with a sensitive
keyword after going into the method call at line \ref{line:fn:src_tag} 
but {\it selectively} bi-directionally propagation policy prevent 
propagating the tag to \varL{id}{line:fn:src_tag}. One possible solution 
for this problem is to record the key-value mappings stored in the 
persistent data storage and share the type set among all variables that 
hold value correlated with the same key.
}

\subsection{Large Scale Evaluation}

In this experiment, we use 10,000 apps not covered by the pilot study.
% experiments.
The apps have a minimum size of 6.46KB for the APK files and a maximum size 
of 49.94MB. The average size of the APK files is 9.17MB. 
Among these apps, there are two that do not contain any DEX bytecode in the 
APK files.
%, \ie they are not detected by \bidtext.
For the remaining apps, the minimum size of the bytecode files 
({\tt classes.dex})  is 452 bytes and the maximum size is 10.32MB. 
The average size of the bytecode files is 2.53MB. 

\subsubsection{Results} \label{sec:subsub:results}
\begin{figure}[t]
	\vspace{1mm}
	\centering
	\includegraphics[angle=-90,width=.98\textwidth]{figs/performance-cdf.eps}
	\caption{Distribution of accumulative analysis time for all apps.}
	\label{fig:performance_cdf}
	\vspace{0.2in}
\end{figure}

\begin{figure}[t]
	\vspace{6mm}
	\centering
	\includegraphics[angle=-90,clip,width=.98\textwidth]{figs/performance-reported.eps}
	\caption{\linespread{1}\selectfont Distribution for the analysis time (in minutes) of the apps reported with sensitive data disclosures.}
	\label{fig:performance_reported}
\end{figure}

\begin{figure}[t]
\centering
\subfloat[][By sources.]{
	\includegraphics[angle=-90,clip,width=.75\textwidth]{figs/breakdown-bytype.eps}
	\label{fig:breakdown_bytype}
}
\quad
\subfloat[position=b][By sinks.]{
	\includegraphics[angle=-90,clip,width=.75\textwidth]{figs/breakdown-sink.eps}
	\label{fig:breakdown_sink}
}
\caption{Breakdown of the reported apps.}
\label{fig:breakdown}
\end{figure}

\begin{figure}[t]
	\centering
\subfloat[][All sinks.]{
	\includegraphics[angle=-90,clip,width=.75\textwidth]{figs/comparison-with.eps}
}
\quad
\subfloat[position=t][Non-logging sinks.]{
	\includegraphics[angle=-90,clip,width=.75\textwidth]{figs/comparison-httponly.eps}
}
	\caption{\linespread{1}\selectfont Comparing \bidtext with static tainting (tracking specific APIs) and SUPOR~\cite{Huang:SUPOR:2015}.}
	\label{fig:comparison_results}
\end{figure}

The total analysis time for the 10,000 apps is 587.6 hours.
Figure~\ref{fig:performance_cdf} presents the distribution of the cumulative 
analysis time for all the 10,000 apps. We divide the total analysis time 
into three parts according to how the analysis on an app terminates. 
As mentioned above, we set the analysis timeout to 20 minutes for each app.
In our evaluation,  856 apps (8.56\%) time out and the 
total analysis time account for 49\% of the total time consumed for the 
10,000 apps. We have 293 other apps of which the analysis ran out of 
memory.
% into errors, \eg exceeding the maximum allowed memory size. 
The total time for these apps accounts for 9\%. For the remaining 
8,852 apps that finished normally take only 42\% of the total analysis 
time. Observe in Figure~\ref{fig:performance_cdf} that the 
first 7,500 apps take less than 15\% of the total time. 
Among the 8,852 apps, the minimum analysis time is 0.2 seconds 
and the maximum time is 1197.4 seconds. The median is 24.9 seconds
while the average time is 99.9 seconds. The largest app that terminates
normally has the APK size of 49.94MB, and the bytecode size of 10.32MB.


Overall, \bidtext reports 4,406 apps with sensitive data disclosure problems. 
We show the analysis time distribution of these apps in Figure~\ref{fig:performance_reported}.
The blue bars show the number of apps that finished within a time period. For 
instance, 472 apps took more than 5 minutes but less than 10 minutes.
We also see that 27 apps timed out in the experiments, although 
partial results were collected before the analysis terminated. 
The red line presents the cumulative analysis time: 93.0\% of the apps 
were analyzed within 10 minutes. We can conclude that \bidtext is 
efficient to be applied to market-scaled apps.

We also show the breakdown of the 4,406 apps by the sources of data disclosures
in Figure~\ref{fig:breakdown_bytype}. 


There are three types of sources:
(1) TEXT -- constant texts in the code that denote sensitive data; (2) API -- 
sensitive API (recall that \bidtext also detects data disclosures originating
from sensitive APIs by associated artificial texts to the source APIs
% manually assigned texts for API source points (\eg 
such as {\tt Location.getLatitude()});
and (3) UI -- constant texts retrieved from user interfaces that denote 
sensitive data.
%{\bf [XZ: it would be nice to show some example here if we have space]}
%\jianjun{Below shows such an example from app {\it com.boxingsport.aircraftmodels}}.
Observe that the majority of disclosures are/can be detected by the 
sensitive text labels. 
Some data disclosure defects can be recognized through multiple sources
%originating from  
%multiple sources 
(\eg TEXT+API), meaning that there are some (bi-directional) data flow paths from a sensitive 
API to a sink and from some constant text to the same sink.  
Consider the following example. 
The data flow path \ref{line:ta:text}$\rightarrow$\ref{line:ta:json}$\rightarrow$\ref{line:ta:sink} denotes a disclosure originating from
TEXT (\ie ``{\it android\_id}'') and the path \ref{line:ta:api}$\rightarrow$\ref{line:ta:json}$\rightarrow$\ref{line:ta:sink}
denotes a disclosure originating from API (\ie ``{\tt getDeviceId()}'').

\lstdefinestyle{textapi}{
	keywords=[2]{json, id},
	keywordstyle=[2]\color{blue}
}
\begin{lstlisting}[style=customlst,style=textapi,escapechar=|]
if (fails_to_obtain_imei()) {
  id = Settings.Secure.getString(resolver, "android_id"); |\label{line:ta:text}|
} else {
  id = telephonyManager.getDeviceId(); |\label{line:ta:api}|
}
json.putString("id", id); |\label{line:ta:json}|
http_sink(json.toString()); // sink |\label{line:ta:sink}|
\end{lstlisting}

The breakdown of the apps by the sink types is shown 
in Figure~\ref{fig:breakdown_sink}. Note that 64.9\% of the reported apps 
contain disclosures due to logging.  Although data disclosure through 
logging is substantially mitigated by access control in the latest version 
of Android, it is still a security concern for legacy Android systems such that
most existing works~\cite{Arzt:2014:FPC:2594291.2594299, Arzt:2014:FPC:2666356.2594299, Kangjie:15, Huang:SUPOR:2015} report these disclosures.
About 38.3\% of the reported apps (16.9\% of all the apps evaluated) 
contain sensitive data disclosures due to to non-logging sinks. They are 
serious threats even in the latest Android systems.

Figure~\ref{fig:comparison_results} shows how \bidtext 
compares with an implementation of the traditional taint tracking technique (tracking disclosures from source APIs 
through forward data-flow similar to~\cite{Gibler:12}) and 
SUPOR~\cite{Huang:SUPOR:2015}, which is a technique that tracks disclosures
from sensitive UI elements (\eg input boxes) through forward data-flow. 
\bidtext always reports a super-set of those reported by the classic tainting
and SUPOR. 
In the figure, the numbers of apps reported by tainting
and SUPOR are normalized to those reported by \bidtext.
Observe that they only report 17.5\% and 53.9\% of those reported by \bidtext,
respectively. Even combining the two can only detect 64.0\%. If only taking non-logging disclosures into account, they report 15.3\% and 60.4\% of those reported by \bidtext.
%\jianjun{check last two sentences. A hole here for two \% numbers.}
This attributes to both the new text label correlation analysis and the 
bi-directional type set propagation strategy.

%53\% of the reported problems are detected by \bidtext. 
%Here we only count the results detected by analyzing code text for \bidtext  category.

\begin{figure}[t]
	\vspace{1mm}
	\centering
	\includegraphics[angle=-90,clip,width=.98\textwidth]{figs/path-distribution.eps}
	\caption{\linespread{1}\selectfont Length distribution of the emitted paths for the reported apps. X-axis shows the length of the paths.}
	\label{fig:path_distribution}
\end{figure}

We present the length distribution of the emitted data disclosure paths 
for the 4,406 apps in Figure~\ref{fig:path_distribution}. 
Though some paths tend to be very long (more than 80 elements), 
most of them are relatively short. More than 75\% of the paths require 
less than 30 steps from the origination of the sensitive texts to 
the sink points.

\noindent
{\bf False Positives and False Negatives.}
It is critical to understand the quality of the reported defects. 
%Since all the apps are randomly selected,
Due to the lack of ground truth, we had to perform manual inspection.
Studying the full set of results is infeasible. Hence, we randomly
chose 100 reported apps with a uniform size distribution for manual 
inspection. The results are presented in Table~\ref{table:evaluation}. 

The columns indicate the sources of the disclosures.
% for the reported apps. 
Row {\em Total} shows the total number of reported 
apps for each sources.
Row {\em Only} shows the number of apps that only have reported 
disclosures falling into one category. 
%where 
%the other two sources do not contribute any reported disclosures. 
The last row shows the number of false positives.

\begin{table}[t]
	\caption{Manually inspected evaluation results for 100 apps.}
	\label{table:evaluation}
	\centering
	\begin{tabular}{|l || r | r | r |}
		\hline
             & {\bf TEXT} & {\bf API} & {\bf UI} \\ \hline \hline
{\bf Total}  & 84         & 22        & 39        \\ \hline
{\bf Only }  & 44         & 2         & 14        \\ \hline
{\bf FP   }  & 3          & 0         & 7         \\ \hline
	\end{tabular}
	\vspace{0.2in}
\end{table}

%Even if overlapping exists in the reported numbers,
Observe that the 10 false positives 
are exclusive. Therefore, the false positive rate is 10\%. The causes for 
false positives will be discussed in Section~\ref{sec:subsub:fp}.
We do not count the false negatives because we don't have the ground 
truth.
%any prior 
%knowledge about the apps.

Among the 84 apps where disclosures are reported by code text analysis, 
62 apps contain paths that can be only detected by our approach via 
text correlation analysis, \ie the 
data used at sink points neither come from any UI inputs nor 
from traditional source APIs. In other words, 62 of them cannot be 
detected by classic tainting or SUPOR. This ratio is consistent with that
in Figure~\ref{fig:comparison_results} for the larger experiment. 
The other reported disclosures have the sensitive data coming from 
these two categories of sources. They are reported by both \bidtext and
the existing technique(s). Another interesting finding is that 
\bidtext often produces a shorter disclosure path. A 
typical scenario is that there is a long data flow path from a UI input
element to a sink. However, mid way through the path, the (sensitive) data
is put/get to/from some container with a sensitive textual key, which 
allows \bidtext to report a shorter path from the put/get operation to the sink.
The benefits of shorter paths are two-folded: less human efforts needed for
inspection and   
% \bidtext finds shorter paths compared
%with the ones starting from the sources, due to the {\it first come first 
%serve} implementation decision mentioned in Section~\ref{sec:impl}.
%One advantage of the shorter paths is that it may 
detecting more disclosures (because the full path from the source points 
to the sink points might be complicated, involving inter-component 
communications, such that the tool may fail to traverse the full path).
% due to  unsuccessfully resolving inter-component communication data flows or 
%limited hardware resources.

\subsubsection{Case Studies} \label{sec:subsub:casestudy}

\comment{
The simplest case for a disclosure detected by analyzing code text exists
in app {\it com.xl.myactivity2}. While the data come from GUI user inputs,
the sink point clearly tells what the data are. That means, even if \bidtext
fails to retrieve the GUI texts, it can easily detect the disclosure problem
by only analyzing the correlated text. The corresponding code snippet is 
as follows:
\begin{lstlisting}[style=customlst]
builder.append(data); // data obtained from user input
Log.v("password", builder.toString();
\end{lstlisting}
}

We observe many cases in which sensitive textual keys appear 
together with data in key-value operations, \eg constructing a name value pair  
(\eg {\it com.gunsound.eddy.fafapro}), inserting data into a 
hash map (\eg {\it me.tango.fishepic}), retrieving/adding 
data to persistent storage through an instance of {\tt SharedPreferences}
(\eg \ {\it com.ifreeindia.sms\_mazaa}) or putting data into a JSON object
(\eg \ {\it com.mobilegustro.war.battle.air.force}). 
\bidtext 
%When the key string 
%of such operations contain sensitive keywords, it is easy to determine 
recognizes the sensitiveness of corresponding data via text correlation 
analysis.

In the following, we show a code snippet adopted from 
app {\it com.}-{\it pro.find.differences} that
%, we find a case similar 
%with the following code snippet that 
discloses sensitive device information to Web servers.

\lstdefinestyle{casestudy}{
	keywords=[2]{aid, resolver},
	keywordstyle=[2]\color{blue}
}
\begin{lstlisting}[style=customlst,style=casestudy,escapechar=|]
void obtainDeviceInfo() {
  TCore.aid = Settings.Secure.getString(resolver, "android_id"); |\label{line:cs:src}|
}
void connectWebServer() {
  Map map = new HashMap();
  safePut(map, "android_id", TCore.aid); |\label{line:cs:callsafeput}|
  String params = convertURLParams(map); // omitted |\label{line:cs:map2params}|
  http_sink(params); // sink |\label{line:cs:sink}|
}
void safePut(Map map, String k, String v) { |\label{line:cs:safeput}|
  map.put(k, v); |\label{line:cs:mapput}|
}
\end{lstlisting}

The method call at line \ref{line:cs:src}
returns system information based 
on the given key value. For example, a unique Id for the device is 
obtained if ``{\it android\_id}'' is given as the key. If the key is 
``{\it enabled\_input\_methods}'', the return value contains a list of 
input methods that are currently enabled. Therefore, the sensitiveness 
of the return value depends on the key. 
%is and it is suitable to 
\bidtext works by
correlating the textual key with the return variable to 
decide whether a later sink operation involves sensitive data or not.

In the above example, the variable {\tt TCore.aid} is typed with 
the constant text ``{\it android\_id}'' at line 
\ref{line:cs:src}, which is later 
propagated to parameter {\tt v} of method {\tt safePut()} 
at line \ref{line:cs:safeput}. {\tt v} is inserted into the hash map
at line \ref{line:cs:mapput}. Note that ``{\it android\_id}'' at 
line \ref{line:cs:callsafeput} is propagated to \varL{k}{line:cs:safeput}
which is further propagated to the hash map and variable {\tt v} 
according to the corresponding API model for propagation. 
Along the data flow, the constant text is propagated to 
\varL{params}{line:cs:map2params} that is eventually used at 
the sink point at line \ref{line:cs:sink}. 
\bidtext reports the data disclosure.
%by applying textual analysis.


\subsubsection{False Positives} \label{sec:subsub:fp}

One of the 10 false positives is caused by unmodeled API functions. 
The corresponding code snippet is from app {\it at.zuggabecka.}-{\it radiofm4}.
%looks like:
\begin{lstlisting}[style=customlst, escapechar=|]
uidx = cursor.getColumnIndex("username"); |\label{line:fp:src_tag}|
iidx = cursor.getColumnIndex("_id");
id = cursor.getLong(iidx); |\label{line:fp:getlong}|
sink(id);
\end{lstlisting}

At line \ref{line:fp:src_tag}, a sensitive keyword ``{\it username}'' 
is correlated with the receiver object {\tt cursor} that is related to a 
database query. Then all uses of {\tt cursor} propagate the text label
to other variables, \eg the return value of a relevant method call.
Thus, {\tt id} at line \ref{line:fp:getlong} is typed with ``{\it username}''.
Later when it is used at a sink point, \bidtext reports a sensitive 
data disclosure after analyzing the corresponding type set.
To remove this false alarm, we can build a model for API 
{\tt Cursor.getColumnIndex(key)} to only propagate type set from {\tt key}
to the return value, avoiding propagating to the receiver object. Then
in the above code snippet, only variable \varL{uidx}{line:fp:src_tag} 
is typed with ``{\it username}''. Variable {\tt id} that appears at 
the sink point is only typed with ``{\it \_id}'' which is not considered 
as a sensitive keyword. Therefore there is no disclosure problem with the model.
%the sink point is not reported.

All the other nine false positives are caused by incorrect 
recognition of text, two for code text and seven for UI text.

App {\it com.netcosports.andalpineski} contains a text label  
``{\it Apps}-{\it \_lang[apps\_lng\_iso2]}'' which indicates the language of 
the app. However, it contains a predefined sensitive keyword ``{\it lng}''
which is mostly used as an abbreviation of ``{\it longitude}''. Failing 
to understand the meaning of the text, \bidtext incorrectly reports a 
sensitive data disclosure.
% when the correlated variables are used at a sink 
%point.

App {\it com.wactiveportsmouthcollege} has a UI text of 
``{\it Pin to desktop}'' where sensitive keyword ``{\it Pin}'' is used 
as a verb. Failing to understand it leads to a false positive. 
All other false positives have similar causes -- sensitive 
keywords in a phrase or sentence do not indicate any sensitive information.
Possible solutions for this type of false positives include integrating more 
advanced NLP techniques with program analysis to understand the meanings 
of the text.

\subsection{Discussion} \label{sec:sub:limitation}

One limitation of \bidtext lies in that the text in code may not be in a 
generalized format. For example, some developers use ``{\it lng}'' for 
``{\it longitude}'' whereas others use 
``{\it long}'' for it, which is a more general word in English. 
If we treat ``{\it long}'' as a sensitive keyword, we can expect many 
false positives. In addition, developers tend to combine several 
words (or abbreviations) into a single word, which makes it more difficult 
to determine whether the correlated data are sensitive or not. 
%Heavy jobs are required to preprocess such kind of texts.
%Compared with that, the texts appearing in GUI are usually in a more 
%readable format. 

%Some future work may improve our approach in several aspects. 
In the future, we plan to improve our approach in the following 
aspects. The first one is to discover text labels in the names 
of method calls, if they are not obfuscated, and variable/field names.
%variable names in source code and field names with 
%in source code or bytecode). 
The second improvement is to consider code comments if source code is 
available. The third one is to improve the NLP aspect by putting the 
keywords in their program context. Doing so, we may be able to 
recognize ``{\it long}'' indeed means longitude.
%We can infer 
%certain information and type it to proper variables by understanding the relevant comments in the code.
